import path from 'path';

const componentsFolderPath = path.join(__dirname, 'app', 'components');
const componentsUI = path.join(__dirname, 'app', 'UI');
const appFolder = path.join(__dirname, 'app');

export default {
  content: [
    path.join(appFolder, '**/*.{js,ts,jsx,tsx,mdx}'),
    path.join(appFolder, '/*.{js,ts,jsx,tsx,mdx}'),
    path.join(appFolder, '.{js,ts,jsx,tsx,mdx}'),
    path.join(componentsFolderPath, '**/*.{js,ts,jsx,tsx,mdx}'),
    path.join(componentsUI, '**/*.{js,ts,jsx,tsx,mdx}'),
  ],
  future: {
    hoverOnlyWhenSupported: true,
  },
  theme: {
    extend: {},
  },
  plugins: [],
};
