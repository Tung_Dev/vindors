// "use client";

// import { Inter } from 'next/font/google';

import LayoutContext from '#/app/components/LayoutContext';

// const inter = Inter({ subsets: ['latin'] });

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <head>
        <link rel='icon' href='/images/logo.png' />
        <meta property="og:image" content="https://vindors.wpengine.com/wp-content/uploads/2023/03/Home-3-Slider-Img-one-1.png"></meta>
      </head>
      <body>
        <LayoutContext>{children}</LayoutContext>
      </body>
    </html>
  );
}
