import axiosClient from './http-common';
import { authEnpoint } from '../../contants/endPoint';

export default {
  login: (payload = {}) => {
    return axiosClient.post(authEnpoint.LOG_IN, payload);
  },
  signUp: (payload = {}) => {
    return axiosClient.post(authEnpoint.SIGN_UP, payload);
  },
  getDetailByToken: () => {
    return axiosClient.get(authEnpoint.BASE_URI_USER);
  },
};
