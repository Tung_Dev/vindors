import type { RootState } from '../store'

export const getData = (state: RootState) => state.authStore.data
export const getLoading = (state: RootState) => state.authStore.loading
export const getError = (state: RootState) => state.authStore.error
