import type { RootState } from '../store'

export const getData = (state: RootState) => state.communicationSentencesStore.data
export const getLoading = (state: RootState) => state.communicationSentencesStore.loading
