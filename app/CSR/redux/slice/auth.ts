import { AnyAction, AsyncThunk, createAction, createSlice } from "@reduxjs/toolkit";
// import type { RootState } from '../store'
import { login, getDetailByToken } from "../thunk/auth";

type GenericAsyncThunk = AsyncThunk<unknown, unknown, any>

type PendingAction = ReturnType<GenericAsyncThunk['pending']>
type RejectedAction = ReturnType<GenericAsyncThunk['rejected']>
type FulfilledAction = ReturnType<GenericAsyncThunk['fulfilled']>


function isPendingAction(action: AnyAction): action is PendingAction {
  return action.type.endsWith('/pending')
}
function isRejectAction(action: AnyAction): action is RejectedAction {
  return action.type.endsWith('/rejected')
}
function isFulfilledAction(action: AnyAction): action is FulfilledAction {
  return action.type.endsWith('/fulfilled')
}

// Define a type for the slice state
interface AuthState {
  data: any;
  loading: string;
  error: any;
}

// Define the initial state using that type
const initialState: AuthState = {
  data: [],
  loading: "idle",
  error: false
};

export const authSlice = createSlice({
  name: "authStore",
  // `createSlice` will infer the state type from the `initialState` argument
  initialState,

  reducers: {
  },
  extraReducers: (builder) => {
    // Add reducers for additional action types here, and handle loading state as needed
    builder
      .addCase(getDetailByToken.fulfilled, (state, action) => {
        state.data = action.payload.data
      })
      .addMatcher(isPendingAction, (state, action) => {
        state.loading = "loading";
      })
      .addMatcher(isRejectAction, (state, action) => {
        state.loading = "idle";
        state.error = action.error;
      })
      .addMatcher(isFulfilledAction, (state, action) => {
        state.loading = "idle";
      })
  },
});

export const actions = authSlice.actions


export default authSlice.reducer;
