import { createAsyncThunk } from "@reduxjs/toolkit";
import { AuthService } from "../../services";

export const login = createAsyncThunk(
  "auth/login",
  async (body: { email: string; password: string }) => {
    return await AuthService.login(body);
  }
);
export const signUp = createAsyncThunk(
  "auth/login",
  async (body: { email: string; password: string, phone:string, name: string }) => {
    return await AuthService.signUp(body);
  }
);

export const getDetailByToken = createAsyncThunk(
  "auth/getDeatailByToken",
  async () => {
    return await AuthService.getDetailByToken();
  }
);



export default {
  login,
  signUp,
  getDetailByToken
};
