import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import {CommunicationSentenceseService} from '../../services'

export const fetchList = createAsyncThunk(
    'communication/fetch',
    async () => {
      return await CommunicationSentenceseService.getAll()
    }
  )

  export default {
    fetchList,
  }