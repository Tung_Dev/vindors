import { combineReducers, configureStore, PreloadedState } from '@reduxjs/toolkit'
import communicationSentencesStore from './slice/communication'
import authStore from './slice/auth'

const rootReducer = combineReducers({
  communicationSentencesStore,
  authStore
})

export const setupStore: any = (preloadedState?: PreloadedState<RootState>) => {
  return configureStore({
    reducer: rootReducer,
    preloadedState
  })
}

export type RootState = ReturnType<typeof rootReducer>
export type AppStore = ReturnType<typeof setupStore>
export type AppDispatch = AppStore['dispatch']