import { bindActionCreators } from '@reduxjs/toolkit';
import { useMemo } from 'react';
import { useSelector } from 'react-redux';
import {actions as actionReducers} from '../slice/auth'
import actionThunks from '../thunk/auth'
import {
    getData,
    getLoading,
    getError
  } from '../selector/auth'
import { useAppDispatch } from '../hooks';


const useAuth = () => {
    const dispatch = useAppDispatch()
    const data = useSelector(getData)
    const loading = useSelector(getLoading)
    const error = useSelector(getError)
    const actions = useMemo(() => bindActionCreators({...actionThunks, ...actionReducers}, dispatch), [])
    return useMemo(() => ({
      actions,
      data,
      loading,
      error
    }), [actions, data, loading, error])
};

export default useAuth;