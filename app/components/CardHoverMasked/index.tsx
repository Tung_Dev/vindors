import React from 'react';
import { Card, Space } from 'antd';
import { WrapCardHoverMasked } from './style';
import { ArrowRightOutlined } from '@ant-design/icons';
import Image from 'next/image';
interface CardHoverMaskedProps {
  src: string;
  text?: string;
  detail?: string;
}

const CardHoverMasked: React.FC<CardHoverMaskedProps> = ({
  src,
  text = '',
  detail,
}) => (
  <WrapCardHoverMasked className="relative w-3/12  px-5 max-md:px-0">
    <div
      className="card-item relative p-0"
    >
      <img src={src} alt={text} className="" width="100" height="100" />

      <div className="card-item-hover center absolute inset-5 box-border flex p-5 text-white">
        <div>
          <div className="text-[26px] tracking-[2.5px]">{text}</div>
          <div className="tracking-[0.05em]">{detail}</div>
        </div>

        <a
          href=""
          className="view-more absolute bottom-8 left-1/2 w-max translate-x-[-50%] text-xs font-semibold uppercase tracking-[2.64px] text-white"
        >
          VIEW MORE <ArrowRightOutlined rev={undefined} />
        </a>
      </div>
    </div>
    <div className="absolute bottom-[45px] left-[45px] text-white">
      <h5 className="text-2xl text-[26px] tracking-[2.5px]">{text}</h5>
      <div className="tracking-[0.05em]">{detail}</div>
    </div>
  </WrapCardHoverMasked>
);

export default CardHoverMasked;
