'use client';

import styled from 'styled-components';
import { primaryColor } from '../../contants/css';

export const WrapCardHoverMasked = styled.div`
  
  @media only screen and (max-width: 767px) {
    padding: 0 10px!important;
  }
  .card-item {
    img {
      width: 100%;
      height: 100%;
    }
    &:hover {
      .card-item-hover {
        animation: all 0.5s ease forwards;
        opacity: 1;
        z-index: 2;
      }
    }
    .card-item-hover {
      opacity: 0;
      transition: all 0.5s ease;
    }
    .card-item-hover {
      background-color: ${primaryColor};
    }
    &::after {
      background-image: linear-gradient(180deg, rgb(255 255 255 / 0%), #000000);
      opacity: 0.8;
      content: '';
      position: absolute;
      bottom: 0;
      width: 100%;
      height: 0;
      transition: all 0.3s ease-in-out;
      height: 160px;
    }
  }
`;
