import styled from "styled-components";

export const WrapBanner = styled.div`
  height: 470px;
  margin-bottom: 200px;
  background-image: url(https://vindors.wpengine.com/wp-content/uploads/2023/04/About-breadcrumb.jpg);

  h1 {
    font-family: "Montserrat", sans-serif;
    font-weight: 400;
    text-transform: uppercase;
    font-size: 50px;
    letter-spacing: 5px;
    color: #000000;
  }
  nav {
    display: flex;
    justify-content: center;
    align-items: center;
  }
`;
