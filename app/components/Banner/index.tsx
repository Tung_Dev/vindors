"use client";
import React, { useState, useEffect } from "react";
import { Breadcrumb } from 'antd';

import { WrapBanner } from './style';

const Banner: React.FC = () => {


  return (
    <WrapBanner className="flex justify-center items-center">
      <div>
        <h1 className="service">WHO WE ARE</h1>
        <Breadcrumb
          separator=">"
          items={[
            {
              title: 'Home',
            },
            {
              title: 'Services',
            },
          ]}
        />
      </div>

    </WrapBanner>
  );
};

export default Banner;
