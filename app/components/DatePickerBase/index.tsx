"use client";
import React, { useState, useEffect } from "react";
import type { DatePickerProps } from 'antd';
import { DatePicker, Space } from 'antd';
import moment from 'moment';

import { WrapDatePickerBase } from './style';

const DatePickerBase: React.FC = () => {



  return (
    <WrapDatePickerBase className="">
      <DatePicker

      />
    </WrapDatePickerBase>
  );
};

export default DatePickerBase;
