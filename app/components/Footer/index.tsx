'use client';
import React, { useState, useEffect, useRef } from 'react';
import { Input } from 'antd';
import {
  ArrowRightOutlined,
  ArrowUpOutlined,
  LinkedinOutlined,
  FacebookOutlined,
  InstagramOutlined,
} from '@ant-design/icons';
import Image from 'next/image';
import { WrapFooter } from './style';
import PhoneIcon from '#/app/contants/icon/PhoneIcon';
import MailIcon from '#/app/contants/icon/MailIcon';
import LocationIcon from '#/app/contants/icon/LocationIcon';

const contacts = [
  {
    icon: <PhoneIcon />,
    text: '1888-707-3648',
  },
  {
    icon: <MailIcon />,
    text: 'info@doitimpact.com​',
  },
  // {
  //   icon: <LocationIcon />,
  //   text: 'No: 7501 E Adamo Dr. Tampa FL, 33619, USA',
  // },
];



const Footer: React.FC = () => {
  const scrollRef = useRef<any>({});

  useEffect(() => {

    if (typeof window !== 'undefined') {
      window.addEventListener(
        'scroll',
        () => {
          if (window.scrollY > 100) {
            scrollRef.current.style.display = 'flex';
          } else {
            scrollRef.current.style.display = 'none';
          }
        },
        false,
      );
    }
  }, []);

  const scrollToTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };
  return (
    <WrapFooter className="relative bg-[#ECF2FB;] pt-[80px] px-[30px]">
      <div
        className="background-overlay opacity-[0.02]"
        style={{
          backgroundImage:
            "url('https://vindors.wpengine.com/wp-content/uploads/2023/03/footer-background.jpg')",
        }}
      ></div>
      <span ref={scrollRef} onClick={scrollToTop} className="trigger-scroll-up">
        <ArrowUpOutlined rev={undefined} />
      </span>
      <div className="relative m-auto flex max-w-[1240px] pb-[80px] max-lg:flex-wrap">
        <div className="flex w-2/5 flex-col pl-2 pr-2 max-lg:w-1/2 max-md:w-full max-md:items-center">
          <div className="min-h-[65px]">
            <Image
              className="-translate-y-[10px]"
              src="/images/logo.png"
              alt="do it impact"
              width="100"
              height="15"
            />
          </div>
          {contacts.map((item, index) => (
            <span key={index} className="mb-4 flex">
              <span className="icon pr-4">{item.icon}</span>
              <span>{item.text}</span>
            </span>
          ))}
          {/* <div className="link-social">
            <span className="mr-[13px]">Social Media</span>
            <div className="icon">
              <LinkedinOutlined rev={undefined} />
            </div>
            <div className="icon">
              <FacebookOutlined rev={undefined} />
            </div>
            <div className="icon">
              <InstagramOutlined rev={undefined} />
            </div>
          </div> */}
        </div>
        <div className="flex w-1/5 flex-col pl-2 pr-2 max-lg:w-1/2 max-md:w-full max-md:items-center">
          <p className="bold-text  min-h-[65px]">Useful Links</p>
          <span className="mb-4">Careers</span>
          <span className="mb-4">FAQ</span>
          <span className="mb-4">E-Catalogue</span>
          <span className="mb-4">Blogs</span>
        </div>
        <div className="flex w-1/6  flex-col pl-2 pr-2 max-lg:w-1/2 max-md:w-full max-md:items-center">
          <p className="bold-text  min-h-[65px]">Main Menu</p>
          <span className="mb-4"><a href="/">Home</a></span>
          <span className="mb-4"><a href="/service">Service</a></span>
          <span className="mb-4"><a href="/about">About Us</a></span>
          <span className="mb-4">Shop</span>
        </div>
        <div className="flex w-1/4 flex-col pl-2 pr-2 max-lg:w-1/2 max-md:w-full max-md:items-center">
          <p className="bold-text  min-h-[65px]">Get in touch</p>

          <Input
            className="span-input mb-4"
            placeholder="Your email"
            suffix={<ArrowRightOutlined rev={undefined} />}
          />
          <span className="mb-4">
            Subscribe to our newsletter for the latest trends and exclusive offers in windows, doors, stairs and complete construction solution
          </span>
        </div>
      </div>
      <div className="all-social">
        © All Copyright 2023 by WeDesignTech
        {/* <img
          src="https://vindors.wpengine.com/wp-content/uploads/2023/03/payment.png"
          alt=""
        /> */}
      </div>
    </WrapFooter>
  );
};

export default Footer;
