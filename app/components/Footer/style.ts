import styled from 'styled-components';

export const WrapFooter = styled.div`
  input {
    padding: 10px !important;
    border-radius: unset;
  }
  textarea {
    border-radius: unset !important;
  }
  .trigger-scroll-up {
    position: fixed;
    bottom: 0;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    font-size: 60px;
    letter-spacing: 2.64px;
    text-transform: uppercase;
    font-weight: 600;
    width: 45px;
    height: 45px;
    border-radius: 50%;
    backdrop-filter: blur(3px) brightness(0.8);
    /* box-shadow: 0 0 0 2px rgb(255, 190, 11, 1); */
    transition: all 0.3s linear 0s;
    cursor: pointer;
    transform: translate(-50%, -50%);
    right: 0;
    color: #ffffff;
    opacity: 0.5;
    z-index: 10000;
    &:hover {
      /* animation: shadowPulse 1.3s linear 1; */
      opacity: 1;
    }
    svg{
      width: 35px;
      height:35px;
    }
  }
  .all-social {
    border-top: 1px solid #dde9ee;
    max-width: 1240px;
    margin: auto;
    font-size: 14px;
    padding: 30px 40px;
  }
  .link-social {
    display: flex;
    padding-top: 30px;
    align-items: center;
    div.icon {
      margin-right: 13px;
      width: 35px;
      height: 35px;
      border-radius: 50%;
      border: 1px solid;
      position: relative;
      .anticon {
        margin: 0;
        display: block;
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
      }
    }
  }
  .span-input {
    border-radius: unset !important;
  }
`;
