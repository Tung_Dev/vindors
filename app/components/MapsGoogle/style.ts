import styled from 'styled-components';
import { primaryColor } from '../../contants/css';

export const WrapMapsGoogle = styled.div`
.map-container {
  width: 100%;
  height: 500px
}
.places-container {
  position: absolute;
  top: 10px;
  left: 50%;
  transform: translateX(-50%);
  z-index: 10;
  width: 300px;
}



.combobox-input {
  width: 100%;
  padding: 0.5rem;
}
.button-locate{
  font-size: 17px;
    line-height: 1.23536;
    font-weight: 400;
    letter-spacing: -.022em;
    font-family: "SF Pro Text","SF Pro Icons","Helvetica Neue","Helvetica","Arial",sans-serif;
    right: 0.88235rem;
    color: #06c;
    position: absolute;
    -webkit-user-select: none;
    user-select: none;
    top: 50%;
    transform: translateY(-50%);
    z-index: 10;
}
.form-icons.form-icons-location {
  background-image: url(data:image/svg+xml;charset=US-ASCII,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2017%2025%22%20fill%3D%22rgb%280%2C102%2C204%29%22%20style%3D%22%22%3E%20%3Ctitle%3Elocation_reduced%401x%3C%2Ftitle%3E%20%3Cg%3E%20%3Crect%20width%3D%2217%22%20height%3D%2225%22%20fill%3D%22none%22%2F%3E%20%3Cpath%20d%3D%22M15.946%2C5.057c-.008.02-.017.043-.027.066L9.235%2C19.511c-.034.073-.075.163-.116.246l-.034-6.57a1.251%2C1.251%2C0%2C0%2C0-.353-.923%2C1.236%2C1.236%2C0%2C0%2C0-.908-.345h0l-6.565-.043c.078-.037.161-.074.236-.108L15.879%2C5.086l.067-.029M16.149%2C4a1.67%2C1.67%2C0%2C0%2C0-.69.178L1.073%2C10.861a2.508%2C2.508%2C0%2C0%2C0-.8.467.86.86%2C0%2C0%2C0-.052%2C1.21%2C1.325%2C1.325%2C0%2C0%2C0%2C.942.337l6.649.043a.3.3%2C0%2C0%2C1%2C.207.052.28.28%2C0%2C0%2C1%2C.06.216l.035%2C6.649a1.325%2C1.325%2C0%2C0%2C0%2C.337.942A.794.794%2C0%2C0%2C0%2C9.017%2C21a.939.939%2C0%2C0%2C0%2C.658-.274%2C2.8%2C2.8%2C0%2C0%2C0%2C.467-.8L16.825%2C5.544c.259-.588.225-1-.069-1.3A.825.825%2C0%2C0%2C0%2C16.149%2C4Z%22%2F%3E%20%3C%2Fg%3E%20%3C%2Fsvg%3E);
    width: 17px;
    height: 25px;
    margin-top: -4px;
    margin-right: 1px;
    display: block;
}
.ant-select.ant-select-auto-complete{
  width: 100%!important;
}
.ant-select-selector{
  height: 41px!important;
  .ant-select-selection-placeholder{
    display: flex;
    align-items: center;
  }
}
.gmnoprint{
  display: none;
}
`;
