'use client';
import { useState } from 'react';
import {
  GoogleMap,
  useLoadScript,
  MarkerF,
  InfoWindowF,
} from '@react-google-maps/api';
import usePlacesAutocomplete, {
  getGeocode,
  getLatLng,
} from 'use-places-autocomplete';
import { AutoComplete } from 'antd';

import { WrapMapsGoogle } from './style';
import { useCallback } from 'react';
import { useEffect } from 'react';
import { Suspense } from 'react';

interface MapsGoogleProps {
  setNearList: any;
}

const MapsGoogle: React.FC<MapsGoogleProps> = ({ setNearList }) => {
  const [selected, setSelected] = useState<any>(null);
  const [center, setCenter] = useState<any>({
    lat: 16.453777,
    lng: 107.592174,
  });
  const [map, setMap] = useState<any>(null);
  const [storeList, setStoreList] = useState([
    {
      id: 1,
      name: '7501 E Adamo Dr. Tampa FL, 33619, USA',
      lat: 27.951684981080344,
      lng: -82.3729056373959,
    },
  ]);
  const maxDistance = 10; // kilometers
  const { isLoaded } = useLoadScript({
    googleMapsApiKey: 'AIzaSyCFhs_UVWvzB4VlY6NM0RF9T4ppRH2HQh8',
    libraries: ['places', 'geometry'],
  });

  const handleMapLoad = useCallback((map: any) => {
    setMap(map);
  }, []);

  useEffect(() => {
    map && map.panTo(selected);
    setNearList(filteredCoordinates);
  }, [selected]);

  // Tính khoảng cách giữa vị trí người dùng và các toạ độ
  const calculateDistance = (
    userLat: any,
    userLng: any,
    coordLat: any,
    coordLng: any,
  ) => {
    const earthRadiusKm = 6371; // Bán kính Trái Đất (kilometers)

    const lat1 = userLat * (Math.PI / 180);
    const lng1 = userLng * (Math.PI / 180);
    const lat2 = coordLat * (Math.PI / 180);
    const lng2 = coordLng * (Math.PI / 180);

    const dLat = lat2 - lat1;
    const dLng = lng2 - lng1;

    const a =
      Math.sin(dLat / 2) ** 2 +
      Math.cos(lat1) * Math.cos(lat2) * Math.sin(dLng / 2) ** 2;
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    const distance = earthRadiusKm * c; // Khoảng cách giữa hai điểm (kilometers)
    return distance;
  };
  // Lọc ra các toạ độ có khoảng cách không quá maxDistance
  const filteredCoordinates = () =>
    storeList.filter((coord: any) => {
      const distance = calculateDistance(
        selected?.lat,
        selected?.lng,
        coord.lat,
        coord.lng,
      );
      return distance <= maxDistance;
    });

  if (!isLoaded) return <div>Loading...</div>;
  const customIconStore = {
    url: 'https://icon-library.com/images/06_1-512_83716.png',
    scaledSize: new window.google.maps.Size(30, 30),
  };
  return (
    <WrapMapsGoogle className="relative">
      <div className="places-container max-md:w-[230px]">
        <PlacesAutocomplete setSelected={setSelected} />
      </div>
      <Suspense fallback={<div>loading...</div>}>
        <GoogleMap
          zoom={11}
          center={{
            lat: 16.453777,
            lng: 107.592174,
          }}
          mapContainerClassName="map-container"
          onLoad={handleMapLoad}
        >
          {storeList.map((coord: any, index: number) => (
            <MarkerF
              key={index}
              position={{ lat: coord.lat, lng: coord.lng }}
              // title={coord.name}
              icon={customIconStore}
            >
              <InfoWindowF
                key={index}
                position={{ lat: coord.lat, lng: coord.lng }}
              >
                <span>{coord.name}</span>
              </InfoWindowF>
            </MarkerF>
          ))}
          {selected && <MarkerF position={selected} />}
        </GoogleMap>
      </Suspense>
    </WrapMapsGoogle>
  );
};

export default MapsGoogle;

const PlacesAutocomplete = ({ setSelected }: any) => {
  const [options, setOptions] = useState<any>([]);
  const [valueInput, setValueInput] = useState('');
  const {
    ready,
    value,
    setValue,
    suggestions: { status, data },
    clearSuggestions,
  } = usePlacesAutocomplete();

  useEffect(() => {
    const newOptions = data.map((item: any, index: number) => ({
      ...item,
      value: item.description,
      label: item.description,
    }));
    setOptions(newOptions);
  }, [data]);

  const handleSelect = async (address: any) => {
    console.log('adress', address);
    setValue(address, false);
    clearSuggestions();

    const results = await getGeocode({ address });
    const { lat, lng } = await getLatLng(results[0]);
    setSelected({ lat, lng });
  };

  const handleGetCurrentLocation = () => {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const { latitude, longitude } = position.coords;
        setSelected({ lat: latitude, lng: longitude });
      });
    } else {
      alert('Your browser not support Geolocation API.');
    }
  };

  return (
    <div className="relative">
      <button className="button-locate" onClick={handleGetCurrentLocation}>
        <span
          className="form-icons form-icons-location"
          id="textbox2_righticon"
        ></span>
      </button>
      {/* <Combobox onSelect={handleSelect}>
        <ComboboxInput
          value={value}
          onChange={(e: any) => setValue(e.target.value)}
          disabled={!ready}
          className="combobox-input"
          placeholder="Search an address"
        />
        <ComboboxPopover>
          <ComboboxList>
            {status === "OK" &&
              data.map(({ place_id, description }) => (
                <ComboboxOption key={place_id} value={description} />
              ))}
          </ComboboxList>
        </ComboboxPopover>
      </Combobox> */}
      <AutoComplete
        // value={valueInput}
        options={options}
        style={{ width: 200 }}
        onSelect={handleSelect}
        onSearch={(text: string) => setValue(text)}
        placeholder="Search location"
      />
    </div>
  );
};
