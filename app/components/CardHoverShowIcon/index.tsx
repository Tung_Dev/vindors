import React from 'react';
import { Card, Space } from 'antd';
import { WrapCardHoverShowIcon } from './style';
import {
  EyeOutlined,
  HeartOutlined,
  ShoppingCartOutlined,
} from '@ant-design/icons';
import Link from 'next/link';

interface CardHoverShowIconProps {
  src: string;
  name: string;
  id?: number;
  link?: string
}

const CardHoverShowIcon: React.FC<CardHoverShowIconProps> = ({
  src,
  name,
  id,
  link
}) => {
  const renderCard = () => (
    <>
      <div className="card-item">
        <img src={src} className="" />
        {/* <div className="white-icon center absolute -bottom-[10px] left-1/3 -translate-x-1/2 p-2 lg:h-10 lg:w-10 lg:w-12 xl:h-12">
  <ShoppingCartOutlined rev={undefined} />
</div> */}
        <div className="white-icon center absolute -bottom-[10px] left-1/2 -translate-x-1/2 p-2 lg:h-10 lg:w-10 lg:w-12 xl:h-12">
          <EyeOutlined rev={undefined} />
        </div>
        {/* <div className="white-icon center absolute -bottom-[10px] left-2/3 -translate-x-1/2 p-2 lg:h-10 lg:w-10 lg:w-12 xl:h-12">
  <HeartOutlined rev={undefined} />
</div> */}
      </div>
      <h5 className="p-[15px] text-center text-[20px] font-medium">{name}</h5>
    </>
  );
  return (
    <WrapCardHoverShowIcon className="card-show-icon relative w-3/12 max-xl:w-1/3 pl-5 pr-5 max-lg:w-1/2 max-md:w-full">
      {link ? (
        <Link href={link}>{renderCard()}</Link>
      ) : (
        renderCard()
      )}
    </WrapCardHoverShowIcon>
  );
};

export default CardHoverShowIcon;
