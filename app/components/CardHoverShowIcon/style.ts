'use client';

import styled from 'styled-components';
import { primaryColor } from '../../contants/css';

export const WrapCardHoverShowIcon = styled.div`
  @keyframes showUp {
    to {
      bottom: 75px;
      opacity: 1;
    }
  }
  &:hover {
    .white-icon {
      &:nth-child(1) {
        animation: showUp 300ms ease 400ms forwards;
      }
      &:nth-child(2) {
        animation: showUp 300ms ease 600ms forwards;
      }
      &:nth-child(3) {
        animation: showUp 300ms ease 800ms forwards;
      }
    }
  }
  .card-item {
    overflow: hidden;
  }
  .white-icon {
    width: 48px;
    height: 48px;
    background-color: #a2aaac;
    border-radius: 50%;
    opacity: 0;
    background-color: #f3f8ff;
    transition: all 100ms ease 0s;
    &:hover {
      cursor: pointer;
    }
    svg {
      width: 24px;
      height: 24px;
      fill: black;
    }
  }
  h5 {
    &:hover {
      color: ${primaryColor};
      cursor: pointer;
    }
  }
`;
