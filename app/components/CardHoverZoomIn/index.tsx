import React, { ReactNode } from 'react';
import { Card, Space } from 'antd';
import { WrapCardHoverZoomIn } from './style';
import { ArrowRightOutlined } from '@ant-design/icons';
interface CardHoverZoomInProps {
  src: string;
  className: string;
  icon: ReactNode;
}

const CardHoverZoomIn: React.FC<CardHoverZoomInProps> = ({
  src,
  icon,
  className,
}) => (
  <WrapCardHoverZoomIn className={className}>
    <div className="relative">
      <a href="" className='center'>
        <img src={src} alt={src} className="" />
        <div className="white-icon center absolute left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2">
          {icon}
        </div>
      </a>
    </div>
  </WrapCardHoverZoomIn>
);

export default CardHoverZoomIn;
