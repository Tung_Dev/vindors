'use client';

import styled from 'styled-components';

export const WrapCardHoverZoomIn = styled.div`
  @keyframes fadeIn {
    to {
      opacity: 0.7;
    }
  }
  overflow: hidden;
  &:hover {
    .white-icon {
      animation: fadeIn 0.3s linear forwards;
    }
    img {
      transform: scale(1.1);
      transition: all 0.5s ease;
    }
  }
  img {
    object-fit: cover;
    object-position: center;
    transition: all 0.5s ease;
  }
  .white-icon {
    width: 50px;
    height: 50px;
    background-color: #a2aaac;
    border-radius: 50%;
    opacity: 0;
    background-color: rgba(0, 0, 0, 0.3);
    svg {
      width: 24px;
      height: 24px;
    }
  }
`;
