'use client';
import React, { useState } from 'react';
import Slider from 'react-slick';
import { WrapCarouselDetailProduct } from './style';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

interface CarouselDetailProductProps {
  arrItems: any;
}

const CarouselDetailProduct: React.FC<CarouselDetailProductProps> = ({
  arrItems,
}) => {
  const [sliderRef] = useState<any | null>({});

  const settings = {
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    dotsClass: 'slick-dots slick-thumb',
    customPaging: function (i: any) {
      return (
        <a>
          <img src={arrItems[i]} alt="" />
        </a>
      );
    },
  };

  return (
    <WrapCarouselDetailProduct>
      <Slider ref={sliderRef} {...settings}>
        {arrItems.map((item: any, index: number) => (
          <div key={index}>
            <img src={item} alt="" />
          </div>
        ))}
      </Slider>
    </WrapCarouselDetailProduct>
  );
};

export default CarouselDetailProduct;
