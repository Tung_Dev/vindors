'use client';

import styled from 'styled-components';
import { primaryColor } from '../../contants/css';
export const WrapHeader = styled.div`
  width: 100%;
  padding: 20px;
  background-image: linear-gradient(
    360deg,
    rgb(255 255 255),
    rgb(255 255 255)
  ) !important;
  position: fixed;
  left: 0;
  top: 0;
  z-index: 1000;
  transition: transform 1s ease;
  box-shadow: 0px 1px 9px 0px #dfe7e7;
  &.my-transparent {
    background-image: unset !important;
    box-shadow: unset !important;
  }
  &.show-menu {
    top: 0px;
  }
  &.hidden-menu {
    transform: translateY(-110px);
  }
  .menu-option {
    display: none;
  }
.consultation-button{
  span{
    font-size: 14px!important;
  }
}
  @media only screen and (max-width: 1310px) {
    .menu-option {
      width: 40px;
    }
    .button-quote {
      flex-grow: 1;
      justify-content: end;
    }
    .horizontal-menu {
      display: none;
    }
    .menu-option {
      display: flex;
    }
  }
  @media only screen and (max-width: 767px) {
    .button-quote {
      display: none !important;
    }
  }
`;
export const CustomMenu = styled.div`
  ul {
    min-width: 1052px;
    border-bottom: unset;
    background: transparent !important;
    li {
      width: fit-content;
      svg {
        width: 11px;
        height: 11px;
      }
      &::after {
        border-bottom-width: 0 !important;
        border-bottom-color: unset !important;
      }

      &.ant-menu-submenu-open {
        .anticon.anticon-down {
          svg {
            transform: rotate(180deg) translateY(2px);
          }
        }
      }
      &.ant-menu-overflow-item {
        padding: 8px;
      }
      span.ant-menu-title-content {
        width: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
        font-weight: 600;
        letter-spacing: 2.64px;
        text-transform: uppercase;
        font-size: 13px;
        a {
          color: unset;
        }
      }
      @media only screen and (min-width: 1400px) {
        &.ant-menu-item{
          padding: 8px 16px!important;
        }
      }
    }
  }
`;

export const CustomButton = styled.div`
  button {
    height: 40px;
    border-radius: unset;
    background-color: ${primaryColor};
    color: black;
    font-weight: 500;
    &:hover {
      background-color: black !important;
    }
  }
`;
