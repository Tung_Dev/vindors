import Commercial from '#/app/contants/icon/Commercial';
import Entry from '#/app/contants/icon/Entry';
import Garage from '#/app/contants/icon/Garage';
import HurricaneDoor from '#/app/contants/icon/HurricaneDoor';
import LiftAndSlide from '#/app/contants/icon/LiftAndSlide';
import MultiSlide from '#/app/contants/icon/MultiSlide';
import Sliding from '#/app/contants/icon/Sliding';
import Swinging from '#/app/contants/icon/Swinging';
import { productCategories } from '#/app/contants/productCategories';
import { DownOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Menu } from 'antd';

interface MenuServerProps {
  mode: any;
  pathname?: string;
}

const MenuServer: React.FC<MenuServerProps> = ({
  mode = 'inline',
  pathname = '',
}) => {
  const renderLabel = (item: any) => {
    const renderContent = () => (
      <div className="flex items-center">
        {item.icon && (
          <span
            className="icon icon-cate-menu mr-[15px]"
            style={{
              backgroundImage: `url(${item.icon})`,
            }}
          ></span>
        )}

        <span className="cate-name">{item.label}</span>
      </div>
    );
    return (
      <>
        {item.link ? (
          <a href={item.link}>{renderContent()}</a>
        ) : (
          renderContent()
        )}
      </>
    );
  };

  const makeProduct = (arrs: any) => {
    return arrs.map((item: any, index: number) => {
      return {
        label: renderLabel(item),
        key: item.key,
        ...(item.children && { children: [...makeProduct(item.children)] }),
        ...(item.popupClassName && { popupClassName: item.popupClassName }),
      };
    });
  };
  const itemss: MenuProps['items'] = [
    {
      label: <a href="/">HOME</a>,
      key: '/',
    },
    {
      label: <a href="/about">ABOUT US</a>,
      key: 'about',
    },
    {
      label: (
        <div>
          PRODUCTS {mode !== 'inline' && <DownOutlined rev={undefined} />}
        </div>
      ),
      key: 'products',
      children: [...makeProduct(productCategories)],
    },
    {
      label: (
        <a href="/service" className='uppercase'>
          Installation
        </a>
      ),
      key: '/services'
    },
    {
      label: (
        <a href="/finance" className='uppercase'>
          Finance
        </a>
      ),
      key: '/finance',
    },
    {
      label: (
        <div className='uppercase'>
          inspiration {mode !== 'inline' && <DownOutlined rev={undefined} />}
        </div>
      ),
      key: '/inspiration',
      children: [
        {
          label: <a href="/gallery" key="/gallery">
            Gallery
          </a>,
          key: 'gallery',
        },
        {
          label: 'Blogs',
          key: 'blogs',
        },
      ],
    },
    {
      label: (
        <div>
          <a href="/contact">
            CONTACT
          </a>
        </div>
      ),
      key: '/contact'
    },
    {
      label: (

        <div>
          <a href="/partner/franchise">PARTNER WITH US</a>
        </div>
      ),
      key: '/partnerWithUs',
    },
  ];
  return (
    <Menu
      defaultSelectedKeys={pathname === '/' ? ['/'] : pathname?.split('/')}
      mode={mode}
      items={itemss}
    />
  );
};

export default MenuServer;
