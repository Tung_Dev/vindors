'use client';
import React, { useEffect, useRef, useState } from 'react';
import { Button, Drawer } from 'antd';
import { usePathname } from 'next/navigation';
import Image from 'next/image';
import { CustomButton, CustomMenu, WrapHeader } from './style';
import { MenuOutlined } from '@ant-design/icons';
import MenuServer from './MenuServer';

const MenuComp: React.FC = () => {
  const pathname = usePathname();
  console.log('pathname', pathname)
  const menuRef = useRef<any>({});
  const [open, setOpen] = useState(false);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  const checkMenuWhite = () => pathname.includes("/partner/contractor") || pathname.includes("/partner/contractor")

  const [mounted, setMounted] = useState(false);
  useEffect(() => {
    setMounted(true);
    if (typeof window !== 'undefined') {
      let lastScrollTop = 0;
      window.addEventListener(
        'scroll',
        () => {
          const scrollTop =
            window.pageYOffset || document.documentElement.scrollTop;

          if (scrollTop > lastScrollTop) {
            // scroll down
            menuRef && menuRef.current?.classList?.remove('show-menu');
            menuRef && menuRef.current?.classList?.add('hidden-menu');
          } else {
            // scroll up
            menuRef && menuRef.current?.classList?.remove('hidden-menu');
            menuRef && menuRef.current?.classList?.add('show-menu');
          }
          if (window.scrollY > 100) {
            menuRef && menuRef.current?.classList?.remove('my-transparent');
          } else if (!checkMenuWhite()) {
            menuRef && menuRef.current?.classList?.add('my-transparent');
          }

          lastScrollTop = scrollTop <= 0 ? 0 : scrollTop;
        },
        false,
      );


    }
  }, []);
  if (!mounted) return <></>;

  return (
    <WrapHeader
      ref={menuRef}
      className={`absolute z-10 w-full pt-5 ${!checkMenuWhite() ? 'my-transparent' : ''}`}
      style={{ pointerEvents: 'all' }}
    >
      <div className="logo m-auto flex max-w-[1604px] justify-between">
        <div className="flex  items-center min-w-[130px]">
          <a href="/" key='/'>
            <Image
              src="/images/logo.png"
              alt="do it impact"
              width="100"
              height="15"
            />
          </a>

        </div>
        <Drawer
          rootClassName="vertical-menu"
          title=""
          placement="right"
          onClose={onClose}
          open={open}
        >
          <MenuServer pathname={pathname} mode="inline" />
        </Drawer>

        <CustomMenu className="horizontal-menu flex flex-1 items-center center">
          <MenuServer pathname={pathname} mode="horizontal" />
        </CustomMenu>


        <CustomButton className="button-quote  flex items-center">
          <a href="/consultation">
            <Button
              className="button-primary h-10 place-items-center consultation-button"
              type="primary"
            >
              CONSULTATION
            </Button>
          </a>

        </CustomButton>
        <div className="menu-option">
          <MenuOutlined
            onClick={showDrawer}
            rev={undefined}
            className="pl-[25px]"
          />
        </div>
      </div>
    </WrapHeader>
  );
};

export default MenuComp;
