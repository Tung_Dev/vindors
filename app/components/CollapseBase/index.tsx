"use client"
import { WrapCollapseBase } from './style';
import { Collapse } from 'antd';
import { PlusOutlined, MinusOutlined } from '@ant-design/icons';

interface CollapseBaseProps {
  items: any;
  accordion?: boolean
}

const CollapseBase: React.FC<CollapseBaseProps> = ({
  items,
  accordion = true
}) => {
  return (
    <WrapCollapseBase>
      <Collapse items={items} accordion defaultActiveKey={['1']} style={{ background: 'unset' }} bordered={false} expandIcon={(props) => props.isActive ? <MinusOutlined rev={undefined} /> : <PlusOutlined rev={undefined} />
      } />
    </WrapCollapseBase>
  );
};

export default CollapseBase;