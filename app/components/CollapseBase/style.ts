'use client';

import styled from 'styled-components';

export const WrapCollapseBase = styled.div`
  .ant-collapse-header-text {
    font-weight: 600;
  }
  .ant-collapse-content-box {
    padding-left: 40px !important;
    padding-right: 40px !important;
  }
  .anticon.anticon-plus.ant-collapse-arrow {
    background: #f5f5f5;
    padding: 5px;
    border-radius: 50%;
  }
  .anticon.anticon-minus.ant-collapse-arrow {
    padding: 5px;
    border-radius: 50%;
    background-color: #444444;
    color: #ffffff !important;
  }
`;
