'use client';

import styled from 'styled-components';
import { primaryColor } from '../../contants/css';
export const WrapCardHoverBlur = styled.div`
  .card-item {
    img {
      width: 100%;
      height: 100%;
    }
    .view-more {
      opacity: 0;
    }
    &:hover {
      .view-more {
        opacity: 1;
        transition: all 0.3s ease-in-out;
        z-index: 2;
      }
      &::after {
        height: 160px;
      }
    }
    &::after {
      background-image: linear-gradient(180deg, rgb(255 255 255 / 0%), ${primaryColor});
      opacity: 0.8;
      content: '';
      position: absolute;
      bottom: 0;
      width: 100%;
      height: 0;
      transition: all 0.3s ease-in-out;
    }
  }
  .category-name{
    &:hover{
      color: ${primaryColor};
    }
  }
`;
