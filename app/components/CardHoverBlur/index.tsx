import React from 'react';
import Image from 'next/image';
import { WrapCardHoverBlur } from './style';
import { ArrowRightOutlined } from '@ant-design/icons';
interface CardHoverBlurProps {
  src: string;
  text: string;
}

const CardHoverBlur: React.FC<CardHoverBlurProps> = ({ src, text }) => (
  <WrapCardHoverBlur className="relative w-3/12 pl-5 pr-5 max-lg:w-1/2 max-md:w-full">
    <div className="card-item relative p-0">
      {/* <Image src={src} alt={text} className="" width="100" height="100" /> */}
      <img src={src} alt={text} className="" />
      <a
        href=""
        className="view-more !text-white letter-spacing-[8px] absolute bottom-8 left-1/2 w-max translate-x-[-50%] text-xs font-semibold uppercase text-white"
      >
        VIEW MORE <ArrowRightOutlined rev={undefined} />
      </a>
    </div>
    <h5 className="mt-10 break-words text-center text-2xl font-light uppercase tracking-[2.5px] max-md:mb-10">
      <a className="category-name" href="">
        {text}
      </a>
    </h5>
  </WrapCardHoverBlur>
);

export default CardHoverBlur;
