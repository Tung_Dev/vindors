
import { Breadcrumb } from 'antd';

import { WrapImageCardHover } from './style';

interface ImageCardHoverProps {
  src: string;
  text: string;
}

const ImageCardHover: React.FC<ImageCardHoverProps> = ({ src, text }) => {


  return (
    <WrapImageCardHover className="pb-20">
      <div>
        <img src={src} alt={src} />
      </div>
      <h5 className="mt-10">{text}</h5>
    </WrapImageCardHover>
  );
};

export default ImageCardHover;
