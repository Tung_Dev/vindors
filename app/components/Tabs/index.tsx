import React from 'react';
import { Tabs as TabsAntd } from 'antd';

import { WrapTabs } from './style';

interface TabsProps {
  items: any;
  className: string;
}

const Tabs: React.FC<TabsProps> = ({ items, ...props }) => {
  return (
    <WrapTabs>
      <TabsAntd {...props} defaultActiveKey="1" items={items} />
    </WrapTabs>
  );
};

export default Tabs;
