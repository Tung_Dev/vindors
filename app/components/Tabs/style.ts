import styled from 'styled-components';

export const WrapTabs = styled.div`
  background-color: red;
  .ant-tabs-nav-wrap {
    justify-content: end;
    font-weight: 600;
    text-transform: capitalize;
    letter-spacing: 0;
    font-size: 22px;
    .ant-tabs-tab {
      margin-left: 60px;
      justify-content: center;
    }

    .ant-tabs-tab-btn {
      padding: 10px 15px;
    }
  }
`;
