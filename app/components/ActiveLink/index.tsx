
import { useRouter } from 'next/navigation'
import { ReactNode } from 'react';
interface ActiveLinkProps {
  children?: ReactNode;
  href?: string;
}

const ActiveLink: React.FC<ActiveLinkProps> = ({ children, href = '' }) => {
  const router = useRouter()

  const handleClick = (e: any) => {
    e.preventDefault()
    router.push(href)
  }

  return (
    <a href={href} onClick={handleClick} >
      {children}
    </a>
  )
};

export default ActiveLink;
