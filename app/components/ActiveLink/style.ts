'use client';

import styled from 'styled-components';
import { primaryColor } from '../../contants/css';

export const WrapCardHoverMasked = styled.div`

  .card-item {
    img {
      width: 100%;
      height: 100%;
    }
    &:hover {
      .card-item-hover {
        animation: all 0.5s ease forwards;
        opacity: 1;
        z-index: 2;
      }
    }
    .card-item-hover {
      opacity: 0;
      transition: all 0.5s ease;
    }
    .card-item-hover {
      background-color: ${primaryColor};
    }
  }
`;
