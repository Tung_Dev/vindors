import React from 'react';

import { Input } from 'antd';

import { WrapInputBase } from './style';

interface InputBaseProps {
  className?: string;
  placeholder?: string;
}

const InputBase: React.FC<InputBaseProps> = ({ className = '', ...props }) => (
  <WrapInputBase className={`${className}`}>
    <Input {...props} />
  </WrapInputBase>
);

export default InputBase;
