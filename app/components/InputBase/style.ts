'use client';

import styled from 'styled-components';

export const WrapInputBase = styled.div`
  input {
    padding: 20px;
    margin-bottom: 30px;
    border-radius: unset;
  }
  textarea {
    border-radius: unset!important;
  }
`;
