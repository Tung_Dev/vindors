'use client';
import React, { useEffect, useState } from 'react';
import Slider from 'react-slick';
import { WrapCarousel } from './style';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Arrow from '#/app/contants/icon/Arrow';

function SampleNextArrow(props: any) {
  const { className, style, onClick } = props;
  return (
    <div className={className} onClick={onClick}>
      <span className="next-text">NEXT</span>
      <Arrow />
    </div>
  );
}

const CarouselFull: React.FC = () => {
  const [sliderRef, setSliderRef] = useState<any | null>({});
  const [isDragging, setIsDragging] = useState(false);
  const [arrItems, setArrItems] = useState([
    {
      src: 'https://vindors.wpengine.com/wp-content/uploads/2023/03/Home-3-Slider-Img-one-1.png',
    },
    {
      src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/Tiny-Slider@2x-2-scaled.jpg',
    },
    {
      src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/Tiny-Slider@2x-3-scaled.jpg',
    },
  ]);

  const settings = {
    infinite: true,
    autoplay: true,
    speed: 1000,
    autoplaySpeed: 3000,
    slidesToShow: 1,
    slidesToScroll: 1,
    beforeChange: (prev: number, next: number) => { },
    nextArrow: <SampleNextArrow />,
  };

  return (
    <WrapCarousel>
      <Slider ref={sliderRef} {...settings}>
        {arrItems.map((item, index) => (
          <div key={index}>{<img src={item.src} alt="" />}</div>
        ))}
      </Slider>
    </WrapCarousel>
  );
};

export default CarouselFull;
