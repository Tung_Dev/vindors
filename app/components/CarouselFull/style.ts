import styled from 'styled-components';
import { primaryColor } from '../../contants/css';

export const WrapCarousel = styled.div`
  .slick-slide {
    img {
      width: 100vw;
      height: 100vh;
      object-fit: cover;
    }
  }
  .slick-arrow.slick-prev{
    display: none!important;
  }
  .slick-arrow.slick-next {
    width: 100px;
    height: fit-content;
    transform: translateX(-80px);
    &::before {
      content: unset;
    }
    &:hover {
      svg {
        fill: ${primaryColor};
      }
      .next-text {
        color: ${primaryColor};
      }
    }
    .next-text {
      font-size: 12px;
      font-weight: 500;
      text-transform: uppercase;
      letter-spacing: 2px;
      color: #ffffff;
      position: absolute;
      top: 50%;
      transform: translate(0, -50%);
    }
    svg {
      height: 100px;
      width: 90px;
      fill: #ffffff;
    }
  }

  @media only screen and (max-width: 768px) {
    .slick-arrow.slick-next {
      display: none;
      svg {
        width: 50px;
        height: 65px;
      }
    }
  }
`;
