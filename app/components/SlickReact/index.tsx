'use client';
import React, { useState, ReactNode, useEffect, useRef } from 'react';
import Slider from 'react-slick';
import { isMobile, isTablet } from 'react-device-detect';

import { WrapSlickReact } from './style';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';


interface SlickReactProps {
  isShowArrow?: boolean;
  isRunningIcon?: boolean;
  total?: number;
  children?: ReactNode;
  settings?: any;
  progress?: boolean;
  className?: string;
  checkWidthToShow?: boolean;
  arrToShowByWidth?: any
}

const SlickReact: React.FC<SlickReactProps> = ({
  isShowArrow = true,
  isRunningIcon,
  total = 4,
  children,
  settings,
  progress,
  className,
  arrToShowByWidth = [2, 4, 5, 6, 8],
  checkWidthToShow
}) => {
  const slidesToShowCheck = settings.slidesToShow ? settings.slidesToShow : isTablet || isMobile ? 1 : total > 4 ? 4 : total - 1 || 1

  const [slidesToShow, setSlidesToShow] = useState(slidesToShowCheck)
  const [progressWidth, setProgressWidth] = useState(100 / (total - slidesToShowCheck + 1));
  const [index, setIndex] = useState(0)
  const [sliderRef, setSliderRef] = useState<any | null>({});
  const [innerSettings, setInnerSettings] = useState({
    speed: 500,
    slidesToScroll: 1,
    swipeToSlide: true,
    infinite: true,
    autoplay: true,
    afterChange: (i: number) => {
      total && setIndex(i)
    },
    cssEase: "linear",
    ...settings,
    slidesToShow: slidesToShow,
  })


  const [mounted, setMounted] = useState(false);
  useEffect(() => {
    setMounted(true);
  }, []);

  const isClient = typeof window === 'object'; //Object represents browser window
  const lastWidth: any = useRef();

  function getSize() {
    return {
      width: isClient ? window.innerWidth : undefined
    }
  }

  const handleResize = () => {
    if (window?.innerWidth !== lastWidth.current) {
      const { width } = getSize();
      lastWidth.current = width;
      if (!width) return
      if (width < 640) {
        setSlidesToShow(1);
      } else if (width >= 640 && width < 768) {
        setSlidesToShow(arrToShowByWidth[0]);
      } else if (width >= 768 && width <= 1024 && total > arrToShowByWidth[1]) {
        setSlidesToShow(arrToShowByWidth[1]);
      } else if (width > 1024 && width <= 1280 && total > arrToShowByWidth[2]) {
        setSlidesToShow(arrToShowByWidth[2]);
      } else if (width > 1280 && width <= 1536 && total > arrToShowByWidth[3]) {
        setSlidesToShow(arrToShowByWidth[3]);
      } else if (width > 1536 && total > arrToShowByWidth[4]) {
        setSlidesToShow(arrToShowByWidth[4]);
      }
    }
  }

  useEffect(() => {

    setInnerSettings({
      ...innerSettings, slidesToShow: slidesToShow
    })
    setIndex(0)
  }, [slidesToShow])
  useEffect(() => {
    total && setProgressWidth((100 / (total - slidesToShow + 1)) * (index + 1))
  }, [index])



  useEffect(() => {
    if (!isClient || !checkWidthToShow) return
    handleResize()
    checkWidthToShow && window.addEventListener('resize', handleResize) // <-- I am only interested in window.innerWidth !
    return () => {
      window.removeEventListener('resize', handleResize)
    }
  }, [])

  if (!mounted) return <></>;

  return (
    <WrapSlickReact className={className} isShowArrow={isShowArrow}>
      <Slider ref={sliderRef} {...innerSettings}>
        {children}
      </Slider>
      {progress && (
        <div className="progress w-3/5 max-md:w-full">
          <span
            style={{
              transform: `translate3d(0px, 0px, 0px) scaleX(${progressWidth / 100
                }) scaleY(1) `,
            }}
            className="progress--fill"
          ></span>
        </div>
      )}
    </WrapSlickReact>
  );
};

export default SlickReact;
