import styled from 'styled-components';
import { primaryColor } from '../../contants/css';
interface WrapSlickReactProps {
  isShowArrow?: boolean;
}

export const WrapSlickReact = styled.div<WrapSlickReactProps>`
  svg {
    &:hover {
      fill: ${primaryColor};
    }
  }
  .slick-slider {
    .slick-arrow {
      display: ${(props) => (props.isShowArrow ? 'block' : 'none')};
    }
  }
  .progress {
    height: 3px;
    margin: auto;
    margin-top: 50px;
    position: relative;
    &::before {
      content: '';
      height: 100%;
      opacity: 0.6;
      position: absolute;
      bottom: 0;
      left: 0;
      right: 0;
      top: 0;
      width: 100%;
      z-index: 1;
      -webkit-border-radius: inherit;
      border-radius: inherit;
      background-image: linear-gradient(
        to right,
        rgba(0, 0, 0, 0) 0%,
        rgba(0, 0, 0, 0.1) 17%,
        rgba(0, 0, 0, 0.1) 83%,
        rgba(0, 0, 0, 0) 100%
      );
    }
    &::after {
      content: '';
      height: 100%;
      opacity: 0.6;
      position: absolute;
      bottom: 0;
      left: 0;
      right: 0;
      top: 0;
      width: 100%;
      z-index: 1;
      -webkit-border-radius: inherit;
      border-radius: inherit;
      background-image: linear-gradient(
        to right,
        rgba(0, 0, 0, 0) 0%,
        rgba(0, 0, 0, 0.1) 17%,
        rgba(0, 0, 0, 0.1) 83%,
        rgba(0, 0, 0, 0) 100%
      );
    }
    .progress--fill {
      width: 100%;
      height: 100%;
      background-color: ${primaryColor};
      transition: 0.3s ease-in;
      position: absolute;
      left: 0;
      top: 0;
      transform-origin: left top;
    }
  }
`;
