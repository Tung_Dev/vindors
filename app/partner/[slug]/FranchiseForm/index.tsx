import React from 'react';
import { Checkbox, Form, Input, Radio, Select } from 'antd';

import { WrapFranchiseForm } from './style';

interface FranchiseFormProps {
  className?: string;
  // onFinish: any
}

const FranchiseForm: React.FC<FranchiseFormProps> = ({ className }) => (
  <WrapFranchiseForm className={className}>
    <Form
      name="form-basic"
      initialValues={{ remember: true }}
      // onFinish={onFinish}
      // onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <div className="flex gap-10 max-md:flex-wrap">
        <div className="w-1/2 max-md:w-full">
          <Form.Item
            // label="First Name"
            name="firtsName"
          // rules={[{ required: true, message: 'Please input your username!' }]}
          >
            <Input placeholder="First Name" />
          </Form.Item>
          <Form.Item
            // label="Phone no"
            name="phoneNo"
          // rules={[{ required: true, message: 'Please input your username!' }]}
          >
            <Input placeholder="Phone no" />
          </Form.Item>
        </div>
        <div className="w-1/2 max-md:w-full">
          <Form.Item
            // label="LastName"
            name="email"
          // rules={[{ required: true, message: 'Please input your username!' }]}
          >
            <Input placeholder='email' />
          </Form.Item>
          <Form.Item
            // label="Email"
            name="lastName"
          // rules={[{ required: true, message: 'Please input your username!' }]}
          >
            <Input placeholder="Last Name" />
          </Form.Item>
        </div>
      </div>
      <Form.Item
        // label="Street Name"
        name="streetName"
      // rules={[{ required: true, message: 'Please input your username!' }]}
      >
        <Input placeholder="Phone no" />
      </Form.Item>
      <div className="flex gap-10 max-md:flex-wrap">
        <div className="w-1/3 max-md:w-full">
          <Form.Item
            // label="City Name"
            name="cityName"
          // rules={[{ required: true, message: 'Please input your username!' }]}
          >
            <Input placeholder="City Name" />
          </Form.Item>
        </div>
        <div className="w-1/3 max-md:w-full">
          <Form.Item
            // label=""
            name="cityName"
          // rules={[{ required: true, message: 'Please input your username!' }]}
          >
            <Select
              className="mb-5 w-full"
              defaultValue="Select State"
              options={[
                { value: 'state1', label: 'State A' },
                { value: 'state2', label: 'State B' },
              ]}
            />
          </Form.Item>
        </div>
        <div className="w-1/3 max-md:w-full">
          <Form.Item
            // label="Zip code"
            name="zipcode"
          // rules={[{ required: true, message: 'Please input your username!' }]}
          >
            <Input placeholder="Phone no" />
          </Form.Item>
        </div>
      </div>
      <Form.Item
        // label="City of my interest"
        name="cityInterest"
      // rules={[{ required: true, message: 'Please input your username!' }]}
      >
        <Input placeholder="City of my interest" />
      </Form.Item>
      <Radio.Group>
        <Radio value={1}>New Franchise</Radio>
        <Radio value={2}>Conversion Franchise</Radio>
      </Radio.Group>
      <Input.TextArea
        placeholder="Message Box"
        style={{ height: '100px!important', resize: 'none', marginTop: 20 }}
      />
      <Checkbox className="!pt-5">Consent</Checkbox>
      <Input.TextArea
        defaultValue="Lorem Ipsum has been the industry's standard add dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book. Lorem Ipsum has been the industry's standard add dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book.Lorem Ipsum has been the industry's standard add dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book.Lorem Ipsum has been the industry's standard add dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book."
        style={{ height: '100px!important', resize: 'none', marginTop: 8 }}
      />
      <div className="mt-10 flex justify-end">
        <button className="button-primary h-10 w-[290px]">SUBMIT</button>
      </div>
    </Form>
  </WrapFranchiseForm>
);

export default FranchiseForm;
