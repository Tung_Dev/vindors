'use client';

import styled from 'styled-components';

export const WrapContractorForm = styled.div`
  .wrap-icon {
    background-color: #efefef;
    height: 171px;
  }
  .box-upload {
    border: 1px dashed #d9d9d9;
    background-color: #fafafa;
    padding: 15px;
    .drop-text {
      color: #5f5f5f;
      font-size: 12px;
      font-weight: 400;
      margin-bottom: 13px;
    }
    .or-click {
      color: #a4a4a4;
      font-size: 10px;
      margin-bottom: 13px;
    }
    .button-add {
      background-color: #4dbd75;
      width: 140px;
      height: 27px;
      color: #ffffff;
      font-size: 13px;
      .plus-add {
        margin-right: 6px;
        width: 13px;
        height: 13px;
        border: 1px solid #ffffff;
        border-radius: 50%;
        transform: translateY(-1px);
      }
    }
  }
`;
