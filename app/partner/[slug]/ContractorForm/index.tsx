import React from 'react';
import { WrapContractorForm } from './style';
import { Form, Input } from 'antd';
import PricingIcon from '#/app/contants/icon/PricingIcon';
import RewardIcon from '#/app/contants/icon/RewardIcon';
import ProductSampleIcon from '#/app/contants/icon/ProductSampleIcon';
import CustomerSupportIcon from '#/app/contants/icon/CustomerSupportIcon';
interface ContractorFormProps {
  className?: string;
}

const contrucorIcon = [
  { icon: <PricingIcon />, name: 'PREFERRED PRICING' },
  { icon: <RewardIcon />, name: 'PERKS & REWARDS' },
  { icon: <ProductSampleIcon />, name: 'product samples' },
  { icon: <CustomerSupportIcon />, name: 'CUSTOMER SUPPORT' },
];

const ContractorForm: React.FC<ContractorFormProps> = ({ className }) => (
  <WrapContractorForm className={className}>
    <div className="flex gap-12 max-md:flex-wrap">
      <div className="w-1/3 max-md:w-full">
        {contrucorIcon.map((item: any, index: number) => (
          <div key={index} className="wrap-icon mb-[23px] pt-[40px]">
            <div className="center mb-[30px]">{item.icon}</div>
            <div className="text-center">{item.name}</div>
          </div>
        ))}
      </div>
      <div className="w-2/3 max-md:w-full">
        <Form
          name="form-basic"
          initialValues={{ remember: true }}
          // onFinish={onFinish}
          // onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            // label="First Name"
            name="companyName"
            // rules={[{ required: true, message: 'Please input your username!' }]}
          >
            <Input placeholder="Company name" />
          </Form.Item>
          <div className="flex gap-10 max-md:flex-wrap">
            <div className="w-1/2 max-md:w-full">
              <Form.Item
                // label="First Name"
                name="businessType"
                // rules={[{ required: true, message: 'Please input your username!' }]}
              >
                <Input placeholder="Business Type" />
              </Form.Item>
              <Form.Item
                // label="First Name"
                name="emailId"
                // rules={[{ required: true, message: 'Please input your username!' }]}
              >
                <Input placeholder="Email Id" />
              </Form.Item>
              <Form.Item
                // label="First Name"
                name="phoneNo"
                // rules={[{ required: true, message: 'Please input your username!' }]}
              >
                <Input placeholder="Phone no" />
              </Form.Item>
              <Form.Item
                // label="First Name"
                name="projectsAnnually"
                // rules={[{ required: true, message: 'Please input your username!' }]}
              >
                <Input placeholder="Projects Annually" />
              </Form.Item>
            </div>
            <div className="w-1/2 max-md:w-full">
              <Form.Item
                // label="First Name"
                name="businessAdress"
                // rules={[{ required: true, message: 'Please input your username!' }]}
              >
                <Input placeholder="Business Adress" />
              </Form.Item>
              <Form.Item
                // label="First Name"
                name="principalOwner"
                // rules={[{ required: true, message: 'Please input your username!' }]}
              >
                <Input placeholder="Principal Owner" />
              </Form.Item>
              <Form.Item
                // label="First Name"
                name="Tax Id"
                // rules={[{ required: true, message: 'Please input your username!' }]}
              >
                <Input placeholder="Tax Id" />
              </Form.Item>
              <Form.Item
                // label="First Name"
                name="WebsiteURL"
                // rules={[{ required: true, message: 'Please input your username!' }]}
              >
                <Input placeholder="Website URL" />
              </Form.Item>
            </div>
          </div>
          <Input.TextArea
            placeholder="Additional Details"
            style={{
              height: '150px!important',
              resize: 'none',
              marginTop: 26,
            }}
          />
          <div className="mb-[25px] mt-10 text-center uppercase">
            Verification Documents (Tax Certificate, Contractor License,
            Business Registration)
          </div>
          <div className="center box-upload flex-col">
            <div className="drop-text">Drop Your Files Here</div>
            <span className="or-click">or click</span>
            <button className="center button-add">
              <span className="center plus-add">+</span>
              Add Files
            </button>
            <input type="file" id="getFile" style={{ display: 'none' }} />
          </div>
          <div className="mt-10 flex justify-end">
            <button className="button-primary h-10 w-[290px]">SUBMIT</button>
          </div>
        </Form>
      </div>
    </div>
  </WrapContractorForm>
);

export default ContractorForm;
