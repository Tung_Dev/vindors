'use client';
import { Checkbox, Form, Input, Radio, Select } from 'antd';
import { WrapFranchise } from './style';
import FranchiseForm from './FranchiseForm';
import ContractorForm from './ContractorForm';

const FranchisePage: React.FC = (props: any) => {
  const { slug } = props.params;
  console.log('slug', slug);
  const onFinish = (values: any) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <WrapFranchise className="wrap-service ">
      <div className="banner center relative min-h-[576px] bg-[#000000d9]">
        <div
          className="background-overlay opacity-[0.1]"
          style={{
            backgroundImage: "url('/images/franchise/Mask_group.png')",
          }}
        ></div>
        <div className="m-auto max-w-[800px] text-center text-white">
          <h1 className="text-[60px] font-semibold uppercase leading-[79px]">
            business opportunity
          </h1>
          <p className="py-10 text-[18px] uppercase">
            Lorem Ipsum has been the industry&apos;s standard add dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book.
          </p>
          <div className="flex justify-center gap-10">
            <a href="/partner/franchise">
              <button
                className={`${slug === 'franchise'
                    ? 'back-ground-primary'
                    : 'border border-solid border-white'
                  }  h-45px  h-10 w-[219px]  text-[18px] uppercase text-white
            `}
              >
                FRANCHISE
              </button>
            </a>
            <a href="/partner/contractor">
              <button
                className={`${slug === 'contractor'
                    ? 'back-ground-primary'
                    : 'border border-solid border-white'
                  } h-10 w-[219px] text-[18px] uppercase text-white`}
              >
                CONTRACTOR
              </button>
            </a>
          </div>
        </div>
      </div>
      <div className=" m-auto max-w-[650px] pb-[38px] pt-[65px] text-center text-[22px] font-semibold uppercase leading-[33px] ">
        {slug === 'franchise'
          ? 'FILL OUT THE FORM TO LEARN MORE ABOUT THE Doitimpact FRANCHISE OPPORTUNITY'
          : slug === 'contractor' &&
          'Contractor Program offers your company an array of benefits in both goods and services, designed to help grow your business'}
      </div>
      <div className="form-section m-auto max-w-[1240px] p-10 px-[100px]">
        {slug === 'franchise' ? (
          <FranchiseForm />
        ) : (
          slug === 'contractor' && <ContractorForm />
        )}
      </div>
    </WrapFranchise>
  );
};

export default FranchisePage;
