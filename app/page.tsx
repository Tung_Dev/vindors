import CardHoverBlur from '#/app/components/CardHoverBlur';
import { CaretRightFilled } from '@ant-design/icons';

import CardHoverMasked from './components/CardHoverMasked';
import SlickReact from './components/SlickReact';
import { TabsProps, Tooltip } from 'antd';
import CardHoverShowIcon from './components/CardHoverShowIcon';

import { WrapHome } from './style';
import StairImpactIcon from './contants/icon/StairImpactIcon';
import DoorImpactIcon from './contants/icon/DoorImpactIcon';
import WindowImpactIcon from './contants/icon/WindowImpactIcon';
import UIIconViewMore from './UI/UIIconViewMore';
import UIInstagram from './UI/UIInstagram';
import CarouselFull from './components/CarouselFull';
import WindowIcon from './contants/icon/WindowIcon';
import UIWhatCustomerSay from './UI/UIWhatCustomerSay';
import { Metadata } from 'next';

const imageOurService = [
  {
    img: '/images/our_services/luxury-home.jpeg',
    text: 'luxury homes',
  },
  {
    img: '/images/our_services/HOTELS.jpeg',
    text: 'hotels',
  },
  {
    img: '/images/our_services/APARTMENTS.jpg',
    text: 'apartments',
  },
  {
    img: '/images/our_services/STOREFRONT.jpeg',
    text: 'storefronts',
  },
];
const imageOldProducts = [
  {
    img: '/images/our_previous_works/1.jpg',
    text: 'BLUE SKY',
    detail: '32 Windows & 4 Doors',
  },
  {
    img: '/images/our_previous_works/2.jpg',
    text: 'Hotels',
    detail: '32 Windows & 4 Doors',
  },
  {
    img: '/images/our_previous_works/3.jpg',
    text: 'Apartments',
    detail: '32 Windows & 4 Doors',
  },
  {
    img: '/images/our_previous_works/5.jpg',
    text: 'Storefront',
    detail: '32 Windows & 4 Doors',
  },
  {
    img: '/images/our_previous_works/6.jpg',
    text: 'Storefront',
    detail: '32 Windows & 4 Doors',
  },
  {
    img: '/images/our_previous_works/7.jpg',
    text: 'Storefront',
    detail: '32 Windows & 4 Doors',
  },
];
const imageOutHistory = [
  {
    img: '/images/home/certificate.png',
  },
  {
    img: '/images/home/certificate.png',
  },
  {
    img: '/images/home/certificate.png',
  },
  {
    img: '/images/home/certificate.png',
  },

  {
    img: '/images/home/certificate.png',
  },
  {
    img: '/images/home/certificate.png',
  },
  {
    img: '/images/home/certificate.png',
  },
  {
    img: '/images/home/certificate.png',
  },
];

const dataPopuparProduct1 = [
  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-4.webp',
    name: 'Doors',
  },
  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-2.webp',
    name: 'Stars',
  },
  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-2.webp',
    name: 'Stars',
  },
  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-6.webp',
    name: 'Fitments',
  },
  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-8.webp',
    name: 'Windows',
  },
  {
    src: '	https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-1.webp',
    name: 'Stars',
  },

  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-7.webp',
    name: 'Windows',
  },
  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-2.webp',
    name: 'Stars',
  },
];
const dataPopuparProduct2 = [
  {
    src: '	https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-5.webp',
    name: 'Doors',
  },

  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-4.webp',
    name: 'Doors',
  },
  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-4.webp',
    name: 'Doors',
  },
  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-2.webp',
    name: 'Stars',
  },
  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-2.webp',
    name: 'Stars',
  },
  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-6.webp',
    name: 'Fitments',
  },
  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-2.webp',
    name: 'Stars',
  },
  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/shop-7.webp',
    name: 'Windows',
  },
];

const iconsDesignService = [
  {
    icon: (
      <UIIconViewMore
        icon={<WindowImpactIcon />}
        name={'Window'}
        viewMore={false}
      />
    ),
  },
  {
    icon: (
      <UIIconViewMore
        icon={<DoorImpactIcon />}
        name={'Door'}
        viewMore={false}
      />
    ),
  },
  {
    icon: (
      <UIIconViewMore
        icon={<StairImpactIcon />}
        name={'Stair'}
        viewMore={false}
      />
    ),
  },
];

const iconsPartnerShip = [
  {
    icon: (
      <UIIconViewMore
        icon={<WindowImpactIcon />}
        name={'Window'}
        viewMore={false}
      />
    ),
  },
  {
    icon: (
      <UIIconViewMore
        icon={<DoorImpactIcon />}
        name={'Door'}
        viewMore={false}
      />
    ),
  },
  {
    icon: (
      <UIIconViewMore
        icon={<StairImpactIcon />}
        name={'Stair'}
        viewMore={false}
      />
    ),
  },

  {
    icon: (
      <UIIconViewMore
        icon={<WindowImpactIcon />}
        name={'Window'}
        viewMore={false}
      />
    ),
  },
  {
    icon: (
      <UIIconViewMore
        icon={<StairImpactIcon />}
        name={'Stair'}
        viewMore={false}
      />
    ),
  },
  {
    icon: (
      <UIIconViewMore
        icon={<DoorImpactIcon />}
        name={'Door'}
        viewMore={false}
      />
    ),
  },
  {
    icon: (
      <UIIconViewMore
        icon={<StairImpactIcon />}
        name={'Stair'}
        viewMore={false}
      />
    ),
  },
  {
    icon: (
      <UIIconViewMore
        icon={<WindowImpactIcon />}
        name={'Window'}
        viewMore={false}
      />
    ),
  },
  {
    icon: (
      <UIIconViewMore
        icon={<DoorImpactIcon />}
        name={'Door'}
        viewMore={false}
      />
    ),
  },
  {
    icon: (
      <UIIconViewMore
        icon={<StairImpactIcon />}
        name={'Stair'}
        viewMore={false}
      />
    ),
  },
];

const iconMainProduct = [
  {
    icon: <WindowImpactIcon />,
    name: 'WINDOWS',
    link: '/products/windows',
  },
  {
    icon: <DoorImpactIcon />,
    name: 'DOORS',
    link: '/products/doors',
  },
  {
    icon: <StairImpactIcon />,
    name: 'STAIRS',
    link: '/products/stairs',
  },
];

const renderTabProducts = (products: any) => (
  <div className="flex flex-wrap">
    {products.map((item: any, index: number) => (
      <CardHoverShowIcon key={index} src={item.src} name={item.name} />
    ))}
  </div>
);

const itemsTabs: TabsProps['items'] = [
  {
    key: '1',
    label: 'Windows',
    children: renderTabProducts(dataPopuparProduct1),
  },
  {
    key: '2',
    label: 'Doors',
    children: renderTabProducts(dataPopuparProduct2),
  },
  {
    key: '3',
    label: 'Stairs',
    children: renderTabProducts(dataPopuparProduct1),
  },
];
export const metadata: Metadata = {
  title: 'Do it impact',
  description: 'Our company specializes in providing doors, stairs, windows',
};
export default async function Index(props: any) {
  return (
    <WrapHome className="wrap-home">
      <div className="relative">
        <div
          className="background-overlay"
          style={{
            backgroundImage:
              "url('https://vindors.wpengine.com/wp-content/uploads/2023/04/home-3-bg.png')",
          }}
        ></div>
        <div className="pb-[80px]">
          <CarouselFull />
        </div>

        <div className="m-auto flex max-w-[1240px] flex-wrap px-[30px] pb-[80px]">
          <div className="relative w-full md:h-[50px] lg:w-2/5">
            {/* <div className="circle absolute right-[-73px] top-[-73px] h-fit w-fit max-lg:hidden w-[150px] h-[150px]">
              <img
                decoding="async"
                loading="lazy"
                width="200"
                height="200"
                src="https://vindors.wpengine.com/wp-content/uploads/2023/04/badge.png"
                alt="badge"
                srcSet="https://vindors.wpengine.com/wp-content/uploads/2023/04/badge.png 200w,  https://vindors.wpengine.com/wp-content/uploads/2023/04/badge-150x150.png 150w,  https://vindors.wpengine.com/wp-content/uploads/2023/04/badge-100x100.png 100w,  https://vindors.wpengine.com/wp-content/uploads/2023/04/badge-50x50.png 50w"
                sizes="(max-width: 200px) 100vw, 200px"
              />
            </div> */}
            <video
              className="max-h-[500px] object-cover max-lg:w-full"
              autoPlay
              muted
              playsInline
              loop
              src="https://vindors.wpengine.com/wp-content/uploads/2023/04/auto-play-1000-x-960.mp4"
            ></video>
          </div>
          <div className="relative w-full lg:w-3/5">
            <div className="pl-[68px] max-md:pl-5">
              <div className="big-text pb-2 !text-[14px] font-semibold uppercase max-md:pt-10 ">
                Commercial - Residential
              </div>
              <div className="big-text uppercase">
                Windows & Doors Manufacturer
              </div>
              <p className="pt-[2px]">
                With two decades in the industry, we're your trusted source for
                top-quality windows, doors, stairs, and accessories. Our
                tailored solutions are designed to elevate luxury homes, hotels,
                apartments, and storefronts.
              </p>
              <div className="pb-[16px] pt-[39px] font-semibold">
                Why To Choose DOITIMPACT
              </div>
              <ul className="ml-[16px]">
                <li className="mb-[16px] list-disc">
                  <span className="font-semibold">Products:</span> Approved by
                  Florida and Miami Dade Building Code!
                </li>
                <li className="mb-[16px] list-disc">
                  <span className="font-semibold">Proven Expertise::</span> With
                  20 years of industry know-how, we deliver products of
                  impeccable craftsmanship.
                </li>
                <li className="mb-[16px] list-disc">
                  <span className="font-semibold">Complete Solutions:</span>
                  From main installations to the finest accessories, find
                  everything you need under one roof.
                </li>
              </ul>
            </div>
          </div>

          {/* <UIcounting
            className="w-full lg:w-3/5 uppercase"
            title="commercial and residential windows and doors manufacturer"
            count1={{
              num: '1,670',
              text: 'Vivamus et odio lacus. ibulules, lacinia augue id, autor',
            }}
            count2={{
              num: '420',
              text: 'Rivamus et odio lacus. ibulules, lacinia augue id, autoe',
            }}
            textButton="KNOW MORE ABOUT US"
          /> */}
        </div>

        <div className="m-auto flex max-w-screen-2xl justify-around pb-[70px] pl-5 pr-5 max-md:flex-wrap">
          {iconMainProduct.map((item, index) => (
            <UIIconViewMore
              className="big-text"
              key={index}
              icon={item.icon}
              name={item.name}
              link={item.link}
            />
          ))}
        </div>
      </div>

      <div className="our-service-section relative  bg-[#EAF0F9] px-[10px] py-[80px]">
        <div
          className="background-overlay bg-auto bg-right-top mix-blend-multiply"
          style={{
            backgroundImage:
              "url('https://vindors.wpengine.com/wp-content/uploads/2023/04/AdobeStock_282470566.jpg')",
          }}
        ></div>
        <div className="ml-[20px] mr-[20px]">
          <div className="big-text mb-5 font-light uppercase">
            our featured Projects
          </div>
          <div className="mb-[50px]">
            Explore our range of quality windows and doors for all building
            types. Let our expert <br /> team bring your vision to life,
            ensuring satisfaction every step of the way.
          </div>
        </div>

        <div className="flex flex-wrap">
          {imageOurService.map((item, index) => (
            <CardHoverBlur key={index} src={item.img} text={item.text} />
          ))}
        </div>
      </div>

      <div className="relative">
        <div
          className="background-overlay"
          style={{
            backgroundImage:
              "url('https://vindors.wpengine.com/wp-content/uploads/2023/03/Group-843.png')",
          }}
        ></div>
        <div className="global-section relative m-auto flex max-w-[1240px] px-[30px] py-[110px] max-md:flex-wrap">
          <div className="center relative w-1/2 flex-1 max-md:w-full max-md:flex-[auto]">
            <div className="location-hover relative m-auto max-w-[650px]">
              <img
                className=""
                src="https://vindors.wpengine.com/wp-content/uploads/2023/03/Group-823.webp"
                alt=""
              />
              <Tooltip
                title={
                  <div>
                    <p>USA</p>
                    <span>12 places</span>
                  </div>
                }
                color={'blue'}
              >
                <div className="location-place absolute bottom-auto left-[21%] top-[40%]"></div>
              </Tooltip>
              <Tooltip
                title={
                  <div>
                    <p>JAMAICA</p>
                    <span>12 places</span>
                  </div>
                }
                color={'blue'}
              >
                <div className="location-place absolute left-[29%] top-[50%]"></div>
              </Tooltip>
              <Tooltip
                title={
                  <div>
                    <p>VIET NAM</p>
                    <span>12 places</span>
                  </div>
                }
                color={'blue'}
              >
                <div className="location-place absolute bottom-auto right-[22%] top-[50%]"></div>
              </Tooltip>
            </div>
          </div>

          <div className="z-[2] w-1/2 pl-10 pr-24 max-lg:pt-7 max-md:w-full max-md:flex-[auto]">
            <div>
              <div className="big-text mb-5 font-light">
                Global <br /> Presence, Local Care
              </div>
              <div className="mb-11">
                We proudly serve customers worldwide, maintaining offices in
                various locations. We specialize in delivering high-quality
                products with punctual service.
              </div>

              <div className="mb-[50px]">
                <ul>
                  <li className="pt-[13px]">
                    <CaretRightFilled rev={undefined} />
                    <span className="pl-3">Customer-Centric Approach</span>
                  </li>
                  <li className="pt-[13px]">
                    <CaretRightFilled rev={undefined} />
                    <span className="pl-3">
                      Uncompromising Quality, Timely Delivery
                    </span>
                  </li>
                  <li className="pt-[13px]">
                    <CaretRightFilled rev={undefined} />
                    <span className="pl-3">Excellence, Every Time</span>
                  </li>
                  <li className="pt-[13px]">
                    <CaretRightFilled rev={undefined} />
                    <span className="pl-3">Reliable Partnerships</span>
                  </li>
                </ul>
              </div>

              <div className="pt-[10px]">
                <button className="button-primary h-10 w-[288px]">
                  <a href="/about" key="/about">
                    KNOW MORE ABOUT US
                  </a>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* section */}

      {/* section */}

      {/* <div className="m-auto max-w-[1640px] bg-[#F3F8FF] p-5 pb-[120px] pt-[120px]">
        <Tabs
          className="tab-popular"
          items={itemsTabs}
          tabBarExtraContent={{
            left: <div className="big-text">POPULAR PRODUCTS</div>,
          }}
        />
      </div> */}

      {/* section */}

      {/* <div className="relative z-[1] bg-[#ECF2FB] pb-[120px] pl-5 pr-5">
        <div
          className="background-overlay z-[-1] opacity-[0.1]"
          style={{
            backgroundImage:
              "url('https://vindors.wpengine.com/wp-content/uploads/2023/04/tab-form-bg.jpg')",
          }}
        ></div> */}

      {/* <div className="m-auto max-w-[750px] pb-[50px] pl-[20px] pr-[20px] max-lg:w-full">
          <div className="big-text mb-[20px] text-center">HAVE A QUERY</div>
          <div className="ml-5 mr-5 text-center">
            Have a question? We're here to help! Drop us a message, and our experts will provide you with the answers and guidance you need.
          </div>
        </div> */}

      {/* HURRICANE */}

      {/* <div className="z-[2] m-auto max-w-[1540px] bg-[#ffffff]">
          <Tabs
            className="tabs-have-a-ques"
            items={[
              {
                key: '1',
                label: 'RAISE A QUERY',
                children: (
                  <div className="p-20 pb-10 max-lg:p-1">
                    <div className="grid grid-cols-[repeat(auto-fill,minmax(48%,1fr))] gap-10">
                      <div>
                        <InputBase placeholder="Your First Name*" />
                        <InputBase placeholder="Your Email*" />
                      </div>
                      <div>
                        <InputBase placeholder="Your Moible*" />
                        <InputBase placeholder="Additional Message" />
                      </div>
                    </div>
                    <button className="button-primary m-auto mt-3 h-10 w-[220px]">
                      SEND MESSAGE
                    </button>
                  </div>
                ),
              },
              {
                key: '2',
                label: 'GET A QUOTE',
                children: <UIGetAQuote />,
              },
              {
                key: '3',
                label: 'NEW SLETTER',
                children: <UINewSletter />,
              },
            ]}
          />
        </div> */}
      {/* </div> */}
      <div className="bg-[#ffffff]">
        <div className="m-auto flex  max-w-[1240px] gap-10 px-[30px] py-[80px] max-md:flex-wrap">
          <div className="w-1/3 max-md:w-full">
            <img
              className="max-md:w-full"
              src="/images/home/hurricane.png"
              alt=""
            />
          </div>
          <div className="w-2/3 pl-[50px] max-md:w-full">
            <h2 className="big-text">
              Florida miami approved hurricane windows
            </h2>
            <p className="py-10">
              Lorem Ipsum is simply dummy text of the printin and typesetting
              <br /> industry. Lorem Ipsum has been the industry's standard
              dummy <br />
              text ever sinc.
            </p>
            <div>
              <button className="button-primary h-10 w-[288px] uppercase">
                <a href="/about" key="/about">
                  View products
                </a>
              </button>
            </div>
          </div>
        </div>
      </div>

      <div className="relative bg-[#ECF2FB] py-[80px] px-[10px]">
        <div
          className="background-overlay opacity-[0.1]"
          style={{
            backgroundImage:
              "url('https://vindors.wpengine.com/wp-content/uploads/2023/04/tab-form-bg.jpg')",
          }}
        ></div>

        <div>
          <div className="m-auto mb-10 max-w-[800px]">
            <div className="big-text m-auto pb-5 text-center uppercase">
              our previous projects
            </div>
            <div className="text-center">
              Explore our past projects in windows, doors, and stairs. Witness
              the fusion of aesthetics <br /> and functionality in every
              installation.
            </div>
          </div>
          <SlickReact
            className="our-pre-pj"
            isShowArrow={false}
            progress
            settings={{ infinite: false }}
            total={imageOldProducts.length}
            checkWidthToShow
            arrToShowByWidth={[2, 3, 4, 4, 4]}
          >
            {imageOldProducts.map((item, index) => (
              <CardHoverMasked
                key={index}
                src={item.img}
                text={item.text}
                detail={item.detail}
              />
            ))}
          </SlickReact>
        </div>
      </div>

      <div className="relative bg-[#ECF2FB] px-[30px] py-[80px]">
        <div
          className="background-overlay"
          style={{
            backgroundImage:
              "url('https://vindors.wpengine.com/wp-content/uploads/2023/03/AdobeStock_204656312.png')",
          }}
        ></div>
        <div className="flex max-lg:flex-wrap">
          <div className="w-1/2 max-lg:w-full">
            <h5 className="big-text mb-5 uppercase">
              Expert Guidance and Custom Design Solutions
            </h5>
            <span>
              Get guidance and custom design solutions from our experts. We're
              here to bring your vision to life, every step of the way.
            </span>
          </div>
          <div className="w-1/2 max-lg:w-full">
            <SlickReact
              settings={{
                autoplay: false,
                speed: 2000,
                autoplaySpeed: 2000,
                arrows: false,
                slidesToShow: 3,
              }}
              // checkWidthToShow
              isRunningIcon={true}
              total={iconsDesignService.length}
              arrToShowByWidth={[2, 3, 4, 4, 4]}
            >
              {iconsDesignService.map((item, index) => (
                <div key={index} className="center icon-expert">
                  {item.icon}
                </div>
              ))}
            </SlickReact>
          </div>
        </div>
      </div>

      {/* certificate */}
      {/* <div className="bg-[#ffffff]">
        <div className="m-auto flex max-w-screen-2xl px-[30px] py-10 max-md:flex-col-reverse max-md:flex-wrap">
          <div className="center flex w-1/2 flex-wrap max-md:w-full">
            {imageOutHistory.map((item, index) => (
              <div
                key={index}
                className="center w-1/4 p-[14px] max-md:w-1/2 max-sm:w-full"
              >
                <img src={item.img} alt="" />
              </div>
            ))}
          </div>
          <div className="w-1/2 max-md:w-full max-md:pb-10 max-md:text-center md:pl-[90px]">
            <h2 className="big-text">US/CA/AU STANDARDS</h2>
            <p className="medium-text m-auto pb-[58px] pt-10 uppercase">
              USA - NFRC / Florida NOA Approval / Miami-Dade Approved / UL
              Standard
              <br />
              Austrialia - AS2047 / AWA I Canada - CSA Certified
            </p>
            <div>
              <button className="button-primary h-10 w-[288px] uppercase">
                <a href="/about" key="/about">
                  certifications
                </a>
              </button>
            </div>
          </div>
        </div>
      </div> */}

      {/* section */}

      <div className="relative bg-[#ffffff] px-[30px] py-[80px]">
        <div className="mb-[87px] text-center">
          <h5 className="big-text mb-5 uppercase">
            Trusted by enterprises globally
          </h5>
        </div>
        <div className="">
          <SlickReact
            settings={{
              autoplay: true,
              speed: 2000,
              autoplaySpeed: 2000,
              cssEase: 'linear',
              arrows: false,
            }}
            checkWidthToShow
            isRunningIcon={true}
            total={iconsPartnerShip.length}
            arrToShowByWidth={[2, 3, 4, 4, 4]}
          >
            {iconsPartnerShip.map((item, index) => (
              <span key={index}>{item.icon}</span>
            ))}
          </SlickReact>
        </div>
      </div>

      <div className="flex px-5 max-md:flex-wrap"></div>

      <div className="bg-[#F3F8FF] pb-[30px]">
        {/* <UIReview /> */}
        <UIWhatCustomerSay className="py-[80px]" />
        <UIInstagram className="bg-[#F3F8FF]" />
      </div>

      {/* section */}

      {/* 
      <video
        src="https://vindors.wpengine.com/wp-content/uploads/2023/04/auto-play-02-2500-x-900.mp4"
        autoPlay
        playsInline
        muted
        loop
      ></video> */}

      {/* <div className='max-w-screen-2xl m-auto flex'>
        <div className='m-w-1/2'></div>

        <div className='m-w-1/2 flex'>
          <div className='m-w-1/2 pr-30px'>
            <img
              decoding='async'
              loading='lazy'
              width='851'
              height='1355'
              src='https://vindors.wpengine.com/wp-content/uploads/2023/05/home-tab-3.1.webp'
              className='attachment-full size-full wp-image-1150'
              alt='home-tab-3.1'
              srcSet='https://vindors.wpengine.com/wp-content/uploads/2023/05/home-tab-3.1.webp 851w, 
        https://vindors.wpengine.com/wp-content/uploads/2023/05/home-tab-3.1-188x300.webp 188w, 
        https://vindors.wpengine.com/wp-content/uploads/2023/05/home-tab-3.1-643x1024.webp 643w, 
        https://vindors.wpengine.com/wp-content/uploads/2023/05/home-tab-3.1-768x1223.webp 768w'
              sizes='(max-width: 851px) 100vw, 851px'
            />
          </div>

          <div className='m-w-1/2 pl-30px'>
            <div>
              <img
                decoding='async'
                loading='lazy'
                width='811'
                height='811'
                src='https://vindors.wpengine.com/wp-content/uploads/2023/05/home-tab-1.2.webp'
                className='attachment-full size-full wp-image-1155'
                alt='home-tab-1.2'
                srcSet='https://vindors.wpengine.com/wp-content/uploads/2023/05/home-tab-1.2.webp 811w, https://vindors.wpengine.com/wp-content/uploads/2023/05/home-tab-1.2-300x300.webp 300w, https://vindors.wpengine.com/wp-content/uploads/2023/05/home-tab-1.2-150x150.webp 150w, https://vindors.wpengine.com/wp-content/uploads/2023/05/home-tab-1.2-768x768.webp 768w, https://vindors.wpengine.com/wp-content/uploads/2023/05/home-tab-1.2-100x100.webp 100w, https://vindors.wpengine.com/wp-content/uploads/2023/05/home-tab-1.2-50x50.webp 50w'
                sizes='(max-width: 811px) 100vw, 811px'
              />
            </div>
            <div>
              <img src='' alt='' />
            </div>
          </div>
        </div>
      </div> */}
    </WrapHome>
  );
}
