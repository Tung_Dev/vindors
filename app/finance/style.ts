'use client';

import styled from 'styled-components';

export const WrapFinance = styled.div`
  .step-box {
    border: 1px solid #d9d9d9;
    padding: 20px;
    position: relative;
    padding-left: 47px;
    .circle-step {
      width: 70px;
      height: 70px;
      display: flex;
      border-radius: 50%;
      justify-content: center;
      align-items: center;
      font-size: 25px;
      position: absolute;
      top: 50%;
      transform: translateY(-50%);
      left: -40px;
      border: 1px solid #d9d9d9;
      z-index: 2;
      background: #ffffff;
    }
  }
`;
