import { WrapFinance } from './style';

import type { CollapseProps } from 'antd';
import CollapseBase from '../components/CollapseBase';

const steps = [
  {
    title: 'Apply in Minutes',
    desc: 'In Simple way, Application will take 5 minutes for instant approval.',
  },
  {
    title: 'Choose The Best Plans',
    desc: 'Checkout loan options. And choose your co-applicants to help in increase approvals.',
  },
  {
    title: 'Manage payments on Greensky.com',
    desc: 'Once approved, You will Recieve the process and reminders from greensky to manage your payments.',
  },
];

const items: CollapseProps['items'] = [
  {
    key: '1',
    label:
      'Uniquely optimize reliable models before wireless results ofessionally impact progressive core.',
    children: (
      <p>
        Professionally impact distributed data via value-added experiences.
        Protraction incentive 24/365 applications whereas turnkey total linkage.
      </p>
    ),
  },
  {
    key: '2',
    label:
      'Uniquely optimize reliable models before wireless results ofessionally impact progressive core.',
    children: (
      <p>
        Professionally impact distributed data via value-added experiences.
        Protraction incentive 24/365 applications whereas turnkey total linkage.
      </p>
    ),
  },
  {
    key: '3',
    label:
      'Uniquely optimize reliable models before wireless results ofessionally impact progressive core.',
    children: (
      <p>
        Professionally impact distributed data via value-added experiences.
        Protraction incentive 24/365 applications whereas turnkey total linkage.
      </p>
    ),
  },
  {
    key: '4',
    label:
      'Uniquely optimize reliable models before wireless results ofessionally impact progressive core.',
    children: (
      <p>
        Professionally impact distributed data via value-added experiences.
        Protraction incentive 24/365 applications whereas turnkey total linkage.
      </p>
    ),
  },
];

const FinancePage: React.FC = () => {
  return (
    <WrapFinance className="wrap-finance  m-auto ">
      <div className="">
        <div>
          <img src="/images/finance/finance_page 1.png" alt="" />
        </div>

        <div className="m-auto max-w-[1240px] px-8 pb-[50px] pl-[70px] pt-[70px] max-md:pl-[58px] max-md:pr-5">
          <div>
            <h1 className="pb-8 text-[40px] leading-[1.2]">
              Get FINANCE in 3 easy steps
            </h1>
            <div className="flex gap-[60px] max-md:flex-wrap">
              {steps.map((item, index) => (
                <div key={index} className="step-box w-1/3 max-md:w-full">
                  <div className="circle-step">{index + 1}</div>
                  <h5 className="mb-[10px] text-[16px] font-semibold">
                    {item.title}
                  </h5>
                  <span>{item.desc}</span>
                </div>
              ))}
            </div>
          </div>
        </div>

        <div className="m-auto flex max-w-[1240px] gap-[25px] px-8 pb-[80px] max-md:flex-wrap max-md:px-5">
          <div className="left w-2/5 pt-[60px] max-md:w-full">
            <img
              className="m-auto w-[75%]"
              src="/images/finance/Frame.png"
              alt=""
            />
          </div>

          <div className="right w-3/5 max-md:w-full">
            <CollapseBase items={items} />
          </div>
        </div>
      </div>
    </WrapFinance>
  );
};

export default FinancePage;
