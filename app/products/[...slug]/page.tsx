import { WrapProducts } from '../style';
import { MenuProps, TabsProps, Tabs } from 'antd';

import HurricaneDoor from '../../contants/icon/HurricaneDoor';
import Commercial from '../../contants/icon/Commercial';
import Entry from '../../contants/icon/Entry';
import Garage from '../../contants/icon/Garage';
import LiftAndSlide from '../../contants/icon/LiftAndSlide';
import MultiSlide from '../../contants/icon/MultiSlide';
import Swinging from '../../contants/icon/Swinging';
import Sliding from '../../contants/icon/Sliding';
import CardHoverShowIcon from '../../components/CardHoverShowIcon';

import { allProducts } from '../../contants/products';
import { productCategories } from '#/app/contants/productCategories';



const renderProducts = (type: string) => (
  <div className="flex flex-wrap">
    {allProducts
      .filter((item) => item.type === type)
      .map((item: any, index: number) => (
        <CardHoverShowIcon
          key={index}
          link={`/products/detail/${item.id}`}
          src={item.src}
          name={item.name}
        />
      ))}
  </div>
);

const renderTabProductss = (arr: any) => {
  return (
    <div className="flex flex-wrap">
      {arr.map((item: any, index: number) => (
        <CardHoverShowIcon
          link={item.link}
          key={index}
          id={item.id}
          src={item.src}
          name={item.name || item.label}
        />
      ))}
    </div>
  );
}

// const itemsTabs: TabsProps['items'] = [
//   {
//     key: 'hurricaneDoors',
//     label: (
//       <div className="flex">
//         <span className="icon">
//           <HurricaneDoor />
//         </span>
//         <span className="cate-name"> Hurricane Doors </span>
//       </div>
//     ),
//     children: renderTabProducts('hurricaneDoors'),
//   },
//   {
//     key: 'commercial',
//     label: (
//       <div className="flex">
//         <span className="icon">
//           <Commercial />
//         </span>
//         <span className="cate-name"> Commercial </span>
//       </div>
//     ),
//     children: renderTabProducts('commercial'),
//   },
//   {
//     key: 'entry',
//     label: (
//       <div className="flex">
//         <span className="icon">
//           <Entry />
//         </span>
//         <span className="cate-name"> Entry </span>
//       </div>
//     ),
//     children: renderTabProducts('entry'),
//   },
//   {
//     key: 'garage',
//     label: (
//       <div className="flex">
//         <span className="icon">
//           <Garage />
//         </span>
//         <span className="cate-name"> Garage </span>
//       </div>
//     ),
//     children: renderTabProducts('garage'),
//   },
//   {
//     key: 'liftAndSlide',
//     label: (
//       <div className="flex">
//         <span className="icon">
//           <LiftAndSlide />
//         </span>
//         <span className="cate-name"> Lift and Slide </span>
//       </div>
//     ),
//     children: renderTabProducts('liftAndSlide'),
//   },
//   {
//     key: 'multiSlide',
//     label: (
//       <div className="flex">
//         <span className="icon">
//           <MultiSlide />
//         </span>
//         <span className="cate-name"> Multi - Slide </span>
//       </div>
//     ),
//     children: renderTabProducts('multiSlide'),
//   },
//   {
//     key: 'swinging',
//     label: (
//       <div className="flex">
//         <span className="icon">
//           <Swinging />
//         </span>
//         <span className="cate-name"> Swinging </span>
//       </div>
//     ),
//     children: renderTabProducts('swinging'),
//   },
//   {
//     key: 'sliding',
//     label: (
//       <div className="flex">
//         <span className="icon">
//           <Sliding />
//         </span>
//         <span className="cate-name"> Sliding </span>
//       </div>
//     ),
//     children: renderTabProducts('sliding'),
//   },
// ];

// {
//   key: 'hurricaneDoors',
//   label: (
//     <div className="flex">
//       <span className="icon">
//         <HurricaneDoor />
//       </span>
//       <span className="cate-name"> Hurricane Doors </span>
//     </div>
//   ),
//   children: renderTabProducts('hurricaneDoors'),
// },
const ProductPage: React.FC = (props: any) => {

  const { slug } = props.params
  const returnItemTabs = () => {
    const checkProductMain = productCategories.filter((item: any) => item.key === slug[0])
    const productMain: any = checkProductMain && checkProductMain.length > 0 ? checkProductMain[0] : null
    let arrTabs: any = []
    if (productMain && slug.length === 1) {

      productCategories.forEach(item => {
        arrTabs.push(
          {
            key: item.key,
            label: (<div className="flex">
              <span className="icon">
                <HurricaneDoor />
              </span>
              <span className="cate-name"> {item.label} </span>
            </div>),
            children: item.children ? renderTabProductss(item.children) : 'have no children'
          }
        )
      })

    } else if (productMain && slug.length === 2) {
      productMain?.children.forEach((item: any) => {
        arrTabs.push(
          {
            key: item.key,
            label: (<div className="flex">
              <span className="icon">
                <HurricaneDoor />
              </span>
              <span className="cate-name"> {item.label} </span>
            </div>),
            children: item.children ? renderTabProductss(item.children) : renderProducts(item.key)
          }
        )
      });
    } else if (productMain && slug.length === 3) {
      const level3 = productMain?.children.filter((item: any) => slug[1] === item.key)
      level3[0].children.forEach((item: any) => {
        arrTabs.push(
          {
            key: item.key,
            label: (<div className="flex">
              <span className="icon">
                <HurricaneDoor />
              </span>
              <span className="cate-name"> {item.label} </span>
            </div>),
            children: renderProducts(item.key)
          }
        )
      });
    }
    return arrTabs
  }



  const itemsTabss: TabsProps['items'] = [
    ...returnItemTabs()
  ]

  return (
    <WrapProducts className="wrap-product">
      <div className="max-w-[1640px] m-auto bg-[#F3F8FF] px-5 pb-[100px] pt-[150px]">

        <Tabs defaultActiveKey={slug[slug.length - 1]} className="tab-popular" items={itemsTabss} tabPosition="left" />
      </div>
    </WrapProducts>
  );
};

export default ProductPage;
