import CarouselDetailProduct from '../../../components/CarouselDetailProduct';

import { WrapProductDetails } from '../../style';
import { allProducts } from '../../../contants/products';
import { Tabs } from 'antd';
import SlickReact from '#/app/components/SlickReact';
import CardHoverMasked from '#/app/components/CardHoverMasked';
import CardHoverShowIcon from '#/app/components/CardHoverShowIcon';

interface ProductDetailPageProps {
  params: any;
}


const itemsTabsDesc = [
  {
    key: '1',
    label: 'DESCRIPTION',
    children: <div className='flex pt-[30px]'>
      <div className='left-desc p-[25px]'>
        <p>Nulla at elit molestie, molestie lacus in, condimentum purus. Orci varius natoque penatibus et magrturient ontes, nascetur ridiculus mus. Sed in auctor massa. Nullam sem nunc, arggdictum quis purus a, ornare lsus. <br /> <br /> Nun empus sem. Quisque neque erat, finibus at ornare in, mollis at dolor. Donec he ndrerit vehicula justo, sed hend endum sit amet. Proin diam felis, tempus ac lacinia vel, aliquam rhoncus neque. Interdum et malesuada fames ac antm.</p>

        <h6 className='medium-text my-[50px]'>INFORMATION</h6>
        <ul>
          <li>Ut scelerisque consequat justo eu dignissim uspen.</li>
          <li>Sed condimentum mattis, accumsan in turpis.</li>
          <li>Ut scelerisque consequat justo eu dignissim uspen.</li>
          <li>Sed condimentum mattis, accumsan in turpis.</li>
        </ul>
      </div>
      <div className='right-desc p-[25px]'>
        <div>
          <img src="https://vindors.wpengine.com/wp-content/uploads/2023/03/shop-desc-img.webp" alt="" />
        </div>
        <h6 className='medium-text mt-[30px] mb-[10px]'>INSTRUCTIONS</h6>
        <div><img className='w-[270px]' src="https://vindors.wpengine.com/wp-content/uploads/2023/03/shop-decs-inner-img.webp" alt="" /></div>
      </div>
    </div>,
  },
  {
    key: '2',
    label: 'ADDITIONAL FEATURES',
    children: <div></div>,
  },
]
const renderTabProducts = (type: string) => (
  <div className="flex flex-wrap">
    {allProducts
      .filter((item) => item.type === type)
      .map((item: any, index: number) => (
        <CardHoverShowIcon
          key={index}
          id={item.id}
          src={item.src}
          name={item.name}
        />
      ))}
  </div>
);

const ProductDetailPage: React.FC<ProductDetailPageProps> = ({ params }) => {
  const { slug } = params;
  const product = allProducts.find((item) => item.id == slug);
  return (
    <WrapProductDetails className="wrap-product-detail ">
      <div className="m-auto max-w-[1640px]  px-5 pb-[100px] pt-[150px]">
        <div className="flex ">
          <div className="left w-[50%] pl-[140px]">
            <CarouselDetailProduct arrItems={product?.subImage} />
          </div>
          <div className="right mb-[10px] pl-[60px]">
            <h1 className="big-text">{product?.name}</h1>
            <p className="mb-[50px]">{product?.desc}</p>
            <h5 className="mb-4 font-semibold">Material</h5>
            <p className="mb-[40px]">Aluminum Teak Wood</p>
            <h5 className="mb-4 font-semibold">Thickness</h5>
            <p className="mb-[40px]">4.2mm/0.2Lbs</p>
            <h5 className="mb-4 font-semibold">Material</h5>
            <p className="mb-[50px]">365 G/ 0.5 pounds</p>
            <h5 className="mb-4 font-semibold">Color</h5>
            <p className="mb-[60px] flex space-x-3">
              <span className="color-item bg-[#4285bf]"></span>
              <span className="color-item bg-[#a5a5a5]"></span>
              <span className="color-item bg-[#81d742]"></span>
              <span className="color-item bg-[#dd3333]"></span>
              <span className="color-item bg-[#eeee22]"></span>
            </p>
            <div>
              <button className="button-primary h-10 px-[50px]">
                <a href="/about" key="/about">
                  GET CONSULTATION OF THIS PRODUCT
                </a>
              </button>
            </div>
          </div>
        </div>

        <div className='desc-section mt-[70px]'>
          <Tabs
            className="tab-description"
            items={itemsTabsDesc}
          />
        </div>

        <div className='mt-[88px]'>
          <SlickReact
            className='react-slider-custom'
            isShowArrow={false}
            settings={{ infinite: false }}
          >
            {allProducts
              .filter((item) => item.type === 'hurricaneDoors')
              .map((item, index) => (
                <CardHoverShowIcon
                  key={index}
                  id={item.id}
                  src={item.src}
                  name={item.name}
                />
              ))}
          </SlickReact>
        </div>
      </div>
    </WrapProductDetails>
  );
};

export default ProductDetailPage;
