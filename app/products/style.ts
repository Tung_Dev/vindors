'use client';
import { primaryColor, secondaryColor } from '../contants/css';

import styled from 'styled-components';

export const WrapProducts = styled.div`
  .tab-popular {
    .ant-tabs-content-holder {
      border: none;
    }
    .ant-tabs-nav {
      box-shadow: 1px 1px 5px 2px #dfe7e7;
      height: fit-content;
      margin-bottom: 50px;
      flex-wrap: wrap;
      padding: 30px;
    }
    .ant-tabs-nav-wrap {
      justify-content: end;
      flex-grow: 1;
      .ant-tabs-ink-bar.ant-tabs-ink-bar-animated {
        display: none;
        background: ${primaryColor};
      }
      .ant-tabs-nav-list {
        .ant-tabs-tab {
          font-size: 16px;
          margin-right: 0;
          display: flex;
          position: relative;
          &:hover {
            background-color: ${primaryColor};
            color: #ffffff;
            path {
              fill: #ffffff;
            }
            &::before,
            &::after {
              width: 100%;
            }
          }
          .icon {
            margin-right: 15px;
            svg {
              width: 30px;
              height: 30px;
            }
          }

          .ant-tabs-tab-btn {
            font-weight: 600;
            /* color: #b7c6dc; */
          }
          .cate-name {
            color: #000000;
          }
          &.ant-tabs-tab-active {
            background-image: unset !important;
            background-color: ${primaryColor};
            .cate-name {
              color: #ffffff;
            }
            path {
              fill: #ffffff;
            }
            &::before {
              width: 100%;
            }
          }
        }
      }
    }
    .card-show-icon {
      margin-bottom: 40px;
    }
  }
`;

export const WrapProductDetails = styled.div`
  .slick-slider.slick-initialized {
    position: relative;
    .slick-dots.slick-thumb {
      position: absolute;
      top: 50%;
      display: flex !important;
      flex-direction: column;
      height: 66%;
      justify-content: space-around;
      transform: translateY(-50%);
      left: -140px;
      width: fit-content;
      li {
        width: 100px;
        height: fit-content;
        &.slick-active {
          box-shadow: 1px 1px 5px 2px #dfe7e7;
        }
      }
    }
  }
  .right {
    span.color-item {
      width: 30px;
      height: 30px;
      border-radius: 50%;
      display: block;
    }
  }
  .left-desc {
    li {
      list-style: disc;
      margin-bottom: 30px;
    }
  }
  .tab-description {
    .ant-tabs-nav-list {
      width: 100%;
      .ant-tabs-ink-bar.ant-tabs-ink-bar-animated {
        background: ${primaryColor};
      }
    }
    .ant-tabs-tab {
      width: 50%;
      justify-content: center;
      font-size: 20px;
      line-height: 2rem;
      font-weight: 400;
      text-align: center;
      text-transform: uppercase;
      margin: 0 !important;
      &:hover {
        color: ${primaryColor};
      }
      &.ant-tabs-tab-active {
        background-image: linear-gradient(
          179deg,
          rgb(255 255 255 / 0%),
          #cff9f9
        ) !important;
        .ant-tabs-tab-btn {
          color: black;
        }
      }
      &:last-child {
        &::after {
          display: none;
        }
      }
    }
    .ant-tabs-content-holder {
      padding: 0;
    }
  }
`;
