import { WrapProducts } from './style';
import { MenuProps, TabsProps, Tabs } from 'antd';

import HurricaneDoor from '../contants/icon/HurricaneDoor';
import Commercial from '../contants/icon/Commercial';
import Entry from '../contants/icon/Entry';
import Garage from '../contants/icon/Garage';
import LiftAndSlide from '../contants/icon/LiftAndSlide';
import MultiSlide from '../contants/icon/MultiSlide';
import Swinging from '../contants/icon/Swinging';
import Sliding from '../contants/icon/Sliding';
import CardHoverShowIcon from '../components/CardHoverShowIcon';

import { allProducts } from '../contants/products';

const items: MenuProps['items'] = [
  {
    label: (
      <div className="flex">
        <span>
          <HurricaneDoor />
        </span>
        <span> Hurricane Doors </span>
      </div>
    ),
    key: '/hurricaneDoors',
  },
  {
    label: (
      <div className="flex">
        <span>
          <Commercial />
        </span>
        <span> Commercial </span>
      </div>
    ),
    key: '/commercial',
  },
  {
    label: (
      <div className="flex">
        <span>
          <Entry />
        </span>
        <span> Entry </span>
      </div>
    ),
    key: '/entry',
  },
  {
    label: (
      <div className="flex">
        <span>
          <Garage />
        </span>
        <span> Garage </span>
      </div>
    ),
    key: '/garage',
  },
  {
    label: (
      <div className="flex">
        <span>
          <LiftAndSlide />
        </span>
        <span> Lift and Slide </span>
      </div>
    ),
    key: '/liftAndSlide',
  },
  {
    label: (
      <div className="flex">
        <span>
          <MultiSlide />
        </span>
        <span> Multi - Slide </span>
      </div>
    ),
    key: '/multiSlide',
  },
  {
    label: (
      <div className="flex">
        <span>
          <Swinging />
        </span>
        <span> Swinging </span>
      </div>
    ),
    key: '/swinging',
  },
  {
    label: (
      <div className="flex">
        <span>
          <Sliding />
        </span>
        <span> Sliding </span>
      </div>
    ),
    key: '/sliding',
  },
];

const renderTabProducts = (type: string) => (
  <div className="flex flex-wrap">
    {allProducts
      .filter((item) => item.type === type)
      .map((item: any, index: number) => (
        <CardHoverShowIcon
          key={index}
          id={item.id}
          src={item.src}
          name={item.name}
        />
      ))}
  </div>
);

const itemsTabs: TabsProps['items'] = [
  {
    key: 'hurricaneDoors',
    label: (
      <div className="flex">
        <span className="icon">
          <HurricaneDoor />
        </span>
        <span className="cate-name"> Hurricane Doors </span>
      </div>
    ),
    children: renderTabProducts('hurricaneDoors'),
  },
  {
    key: 'commercial',
    label: (
      <div className="flex">
        <span className="icon">
          <Commercial />
        </span>
        <span className="cate-name"> Commercial </span>
      </div>
    ),
    children: renderTabProducts('commercial'),
  },
  {
    key: 'entry',
    label: (
      <div className="flex">
        <span className="icon">
          <Entry />
        </span>
        <span className="cate-name"> Entry </span>
      </div>
    ),
    children: renderTabProducts('entry'),
  },
  {
    key: 'garage',
    label: (
      <div className="flex">
        <span className="icon">
          <Garage />
        </span>
        <span className="cate-name"> Garage </span>
      </div>
    ),
    children: renderTabProducts('garage'),
  },
  {
    key: 'liftAndSlide',
    label: (
      <div className="flex">
        <span className="icon">
          <LiftAndSlide />
        </span>
        <span className="cate-name"> Lift and Slide </span>
      </div>
    ),
    children: renderTabProducts('liftAndSlide'),
  },
  {
    key: 'multiSlide',
    label: (
      <div className="flex">
        <span className="icon">
          <MultiSlide />
        </span>
        <span className="cate-name"> Multi - Slide </span>
      </div>
    ),
    children: renderTabProducts('multiSlide'),
  },
  {
    key: 'swinging',
    label: (
      <div className="flex">
        <span className="icon">
          <Swinging />
        </span>
        <span className="cate-name"> Swinging </span>
      </div>
    ),
    children: renderTabProducts('swinging'),
  },
  {
    key: 'sliding',
    label: (
      <div className="flex">
        <span className="icon">
          <Sliding />
        </span>
        <span className="cate-name"> Sliding </span>
      </div>
    ),
    children: renderTabProducts('sliding'),
  },
];

const ProductPage: React.FC = () => {
  return (
    <WrapProducts className="wrap-product">
      {/* <div className="max-w-[1640px] m-auto bg-[#F3F8FF] px-5 pb-[100px] pt-[150px]">

        <Tabs className="tab-popular" items={itemsTabs} tabPosition="left" />
      </div> */}
      no use now
    </WrapProducts>
  );
};

export default ProductPage;
