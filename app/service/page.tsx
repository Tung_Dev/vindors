import DoorImpactIcon from '../contants/icon/DoorImpactIcon';
import StairImpactIcon from '../contants/icon/StairImpactIcon';
import WindowImpactIcon from '../contants/icon/WindowImpactIcon';
import { WrapService } from './style';

const iconMainProduct = [
  {
    icon: <WindowImpactIcon />,
    name: 'WINDOWS',
    desc: "Lorem Ipsum has been the industry's standard dummy text ever",
    link: '',
  },
  {
    icon: <DoorImpactIcon />,
    name: 'DOORS',
    desc: "Lorem Ipsum has been the industry's standard dummy text ever",
    link: '',
  },
  {
    icon: <StairImpactIcon />,
    name: 'STAIRS',
    desc: "Lorem Ipsum has been the industry's standard dummy text ever",
    link: '',
  },
];

const ServicePage: React.FC = () => {
  return (
    <WrapService className="wrap-service pt-[100px]">
      <div className="banner flex h-[500px]">
        <div className="left relative w-2/3">
          <div
            className="background-overlay opacity-[0.1]"
            style={{
              backgroundImage: "url('/images/service/chair.png')",
            }}
          ></div>
          <div className="wrap-text max-w-[525px] absolute right-[266px] top-[26px]">
            <h1 className="main-text mb-[37px] max-w-[479px] max-md:mb-[25px] max-md:!text-[21px] max-md:!leading-[33px]">
              WINDOWS AND DOORS BUILT FOR{' '}
              <span className="main-text tracking-[0] text-[#09858b] max-md:!text-[21px] max-md:!leading-[33px]">
                HOW YOU LIVE
              </span>
            </h1>
            <p className="mb-[44px] max-w-[439px] text-[16px] leading-[24px] tracking-[1px]">
              Lorem Ipsum has been the industry&apos;s standard dummy text ever
              since the 1500s, when an unknown printer took a galley of type.
            </p>
            <div>
              <button className="button-primary h-[36px] w-[172px] text-[14px] !normal-case leading-[21px]">
                <a>View Products</a>
              </button>
            </div>
          </div>
        </div>
        <div className="right relative w-1/3">
          <img
            className="h-[479px] w-[439px] max-md:h-[200px]"
            src="/images/service/wind.png"
            alt=""

          />
        </div>
      </div>

      <div className="px-10">
        <div className="m-auto max-w-[1640px]">
          <p className="m-auto max-w-[950px] pb-[97px] pt-[100px] text-center leading-[24px]">
            Lorem Ipsum has been the industry&apos;s standard add dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book.Lorem Ipsum has been
            the industry&apos;s standard dummtext ever since the 1500s, when an
            unknown printer tookgalley of type and scrambled it to make a type
            specimen book.Lorem Ipsum has been.
          </p>
        </div>
        <div className="m-auto max-w-[1640px]">
          <h4 className="m-auto mb-[22px] text-center text-[36px] font-semibold uppercase leading-[54px]">
            Professional SERVICES
          </h4>
          <p className="m-auto max-w-[924px] pb-[50px] text-center leading-[24px]">
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry
          </p>
        </div>
        <div className="product mb-[60px] flex gap-8 max-md:flex-wrap">
          {iconMainProduct.map((item: any, index: any) => {
            return (
              <div key={index} className="wrap-item w-1/3 max-md:w-full">
                <div className="icon center mb-[25px] mt-[31px]">
                  {item.icon}
                </div>
                <div className="bold-text mb-[16px]">{item.name}</div>
                <p className="max-w-[264px] text-center">{item.desc}</p>
                <button className="my-[38px] ">view product</button>
              </div>
            );
          })}
        </div>
      </div>
    </WrapService>
  );
};

export default ServicePage;
