'use client';

import styled from 'styled-components';
import { primaryColor } from '#/app/contants/css';
export const WrapService = styled.div`
  .banner {
    .left {
      .wrap-text {
        padding-top: 50px;
        padding-left: 40px;
        padding-right: 20px;
      }
      .main-text {
        font-size: 36px;
        font-weight: 600;
        line-height: 54px;
      }
    }
    .right {
      background-image: linear-gradient(210deg, #000000, #576c72);
      img {
        position: absolute;
        left: -349px;
        z-index: 10;
        bottom: -93px;
        transform: translateX(46px);
      }
    }
  }
  .product {
    .wrap-item {
      background-color: #fafafa;
      display: flex;
      flex-direction: column;
      align-items: center;
      button {
        width: 117px;
        border: 1px solid #6e6a6a;
      }
      &:hover {
        background-color: ${primaryColor};
        color: #ffffff;
        button {
          background-color: ${primaryColor};
          border: 1px solid #ffffff;
        }
        .icon {
          border: 2px solid #08787d;
          background-color: #229197;
          svg {
            width: 44px !important;
            height: 44px !important;
            fill: #ffffff;
          }
        }
      }
    }
    .icon {
      width: 88px;
      height: 88px;
      border-radius: 50%;
      background-color: #f3f3f3;
      svg {
        width: 44px !important;
        height: 44px !important;
      }
    }
  }
`;
