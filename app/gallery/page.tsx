import { WrapGalleryPage } from './style';
import { TabsProps, Tabs } from 'antd';

import HurricaneDoor from '#/app/contants/icon/HurricaneDoor';
import CardHoverShowIcon from '#/app/components/CardHoverShowIcon';

import { allProducts, galleryImages } from '#/app/contants/products';
import { galleryCate } from '#/app/contants/productCategories';



const renderProductGallery = (type: string) => (
  <div className="flex flex-wrap">
    {galleryImages
      // .filter((item) => item.type === type)
      .map((item: any, index: number) => (
        <CardHoverShowIcon
          key={index}
          link={`/products/detail/${item.id}`}
          src={item.src}
          name={item.name}
        />
      ))}
  </div>
);

const renderTabProductss = (arr: any) => {
  return (
    <div className="flex flex-wrap">
      {arr.map((item: any, index: number) => (
        <CardHoverShowIcon
          link={item.link}
          key={index}
          id={item.id}
          src={item.src}
          name={item.name || item.label}
        />
      ))}
    </div>
  );
}

const GalleryPage: React.FC = (props: any) => {

  const returnItemTabs = () => {
    return galleryCate.map(item => {
      return {
        key: item.key,
        label: (<div className="flex">
          <span className="icon">
            <HurricaneDoor />
          </span>
          <span className="cate-name"> {item.label} </span>
        </div>),
        children: renderProductGallery(item.key)
      }
    })

  }



  const itemsTabss: TabsProps['items'] = [
    ...returnItemTabs()
  ]

  return (
    <WrapGalleryPage className="wrap-product">
      <div className="max-w-[1640px] m-auto bg-[#F3F8FF] px-5 pb-[100px] pt-[150px]">

        <Tabs className="tab-popular" items={itemsTabss} tabPosition="left" />
      </div>
    </WrapGalleryPage>
  );
};

export default GalleryPage;
