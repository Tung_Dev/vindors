'use client';

import styled from 'styled-components';
import { primaryColor, secondaryColor } from './contants/css';

export const WrapHome = styled.div`
  background-color: #f3f8ff;
  .hover-blue-icon {
    i {
      display: flex;
      justify-content: center;
    }
  }
  .ant-tabs-content-holder {
    padding: 32px 0;
  }
  .ant-tabs-nav-list {
    .ant-tabs-tab {
      &:hover {
        color: ${primaryColor};
        &::before,
        &::after {
          width: 100%;
        }
      }
      &::before {
        background-color: ${primaryColor};
        left: 0;
        bottom: -2px;
        width: 0;
        height: 5px;
        position: absolute;
        margin: auto;
        content: '';
        color: #b7c6dc;
        transition: width 0.8s cubic-bezier(0.22, 0.61, 0.36, 1);
      }
      &::after {
        background-color: ${primaryColor};
        left: 0;
        bottom: -2px;
        width: 0;
        height: 5px;
        position: absolute;
        margin: auto;
        content: '';
        transition: width 0.8s cubic-bezier(0.22, 0.61, 0.36, 1);
      }
      .ant-tabs-tab-btn {
        &:hover {
          color: ${primaryColor};
        }
      }
      &.ant-tabs-tab-active {
        &::before {
          width: 100%;
        }
      }
    }
  }
  .tab-popular {
    .ant-tabs-nav {
      margin-bottom: 50px;
      flex-wrap: wrap;
    }
    .ant-tabs-nav-wrap {
      justify-content: end;
      flex-grow: 1;
      .ant-tabs-ink-bar.ant-tabs-ink-bar-animated {
        display: none;
        background: ${primaryColor};
      }
      .ant-tabs-nav-list {
        width: 90%;
        justify-content: space-between;
        .ant-tabs-tab {
          font-size: 22px;
          margin-right: 0;
          width: 25%;
          display: flex;
          justify-content: center;
          position: relative;
          max-width: 140px;
          &:hover {
            color: ${primaryColor};
            &::before,
            &::after {
              width: 100%;
            }
          }
          .ant-tabs-tab-btn {
            font-weight: 600;
            color: #b7c6dc;
            &:hover {
              color: ${primaryColor};
            }
          }
          &.ant-tabs-tab-active {
            background-image: unset !important;
            .ant-tabs-tab-btn {
              color: ${primaryColor};
            }
            &::before {
              width: 100%;
            }
          }
        }
      }
    }
    .card-show-icon {
      margin-bottom: 40px;
    }
  }
  .tabs-have-a-ques {
    .ant-tabs-nav-list {
      width: 100%;
      .ant-tabs-ink-bar.ant-tabs-ink-bar-animated {
        background: ${primaryColor};
      }
    }
    .ant-tabs-tab {
      width: 33.33333333%;
      justify-content: center;
      font-size: 20px;
      line-height: 2rem;
      font-weight: 400;
      text-align: center;
      text-transform: uppercase;
      margin: 0 !important;
      &:hover {
        color: ${primaryColor};
      }
      &.ant-tabs-tab-active {
        background-image: linear-gradient(
          179deg,
          rgb(255 255 255 / 0%),
          #cff9f9
        ) !important;
        .ant-tabs-tab-btn {
          color: black;
        }
      }
      &:last-child {
        &::after {
          display: none;
        }
      }
    }
    .ant-tabs-content-holder {
      padding: 0;
    }
  }
  .our-history {
    .slick-slide {
      padding: 0 30px;
    }
  }


  @media only screen and (max-width: 767px) {
    .ant-tabs-nav-wrap {
      justify-content: unset;
      .ant-tabs-tab {
        margin-left: unset;
        margin-right: 18px;
        font-size: 18px;
        word-wrap: break-word;
      }
    }
    .tab-popular {
      .ant-tabs-tab {
        margin-left: unset;
        margin-right: 18px;
        font-size: 18px;
        word-wrap: break-word;
      }
    }
    .tabs-have-a-ques {
      .ant-tabs-tab {
        font-size: 14px;
        padding: 12px;
        margin: 0 !important;
      }
    }
  }
  .impact-icon-product.big-text {
    h5 {
      font-size: 20px;
    }
  }
  .icon-expert {
    .impact-icon-product {
      width: 100%;
    }
  }
`;
