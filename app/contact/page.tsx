'use client';
import { WrapConsultation } from './style';

import { CollapseProps, Input } from 'antd';
import FaceBookColorIcon from '../contants/icon/FaceBookColorIcon';
import InstagramColorIcon from '../contants/icon/InstagramColorIcon';
import LinkInColorIcon from '../contants/icon/LinkInColorIcon';
import CollapseBase from '../components/CollapseBase';
import PhoneIcon from '../contants/icon/PhoneIcon';
import MailIcon from '../contants/icon/MailIcon';
import LocationIcon from '../contants/icon/LocationIcon';
import GoogleMaps from '../components/GoogleMaps';
import MapsGoogle from '../components/MapsGoogle';
import { useState } from 'react';

const contacts = [
  {
    icon: <PhoneIcon />,
    text: '1888-707-3648',
  },
  {
    icon: <MailIcon />,
    text: 'info@doitimpact.com​',
  },
  {
    icon: <LocationIcon />,
    text: '7501 E Adamo Dr. Tampa FL, 33619, USA',
  },
];

const items: CollapseProps['items'] = [
  {
    key: '1',
    label:
      'Uniquely optimize reliable models before wireless results ofessionally impact progressive core.',
    children: (
      <p>
        Professionally impact distributed data via value-added experiences.
        Protraction incentive 24/365 applications whereas turnkey total linkage.
      </p>
    ),
  },
  {
    key: '2',
    label:
      'Uniquely optimize reliable models before wireless results ofessionally impact progressive core.',
    children: (
      <p>
        Professionally impact distributed data via value-added experiences.
        Protraction incentive 24/365 applications whereas turnkey total linkage.
      </p>
    ),
  },
  {
    key: '3',
    label:
      'Uniquely optimize reliable models before wireless results ofessionally impact progressive core.',
    children: (
      <p>
        Professionally impact distributed data via value-added experiences.
        Protraction incentive 24/365 applications whereas turnkey total linkage.
      </p>
    ),
  },
  {
    key: '4',
    label:
      'Uniquely optimize reliable models before wireless results ofessionally impact progressive core.',
    children: (
      <p>
        Professionally impact distributed data via value-added experiences.
        Protraction incentive 24/365 applications whereas turnkey total linkage.
      </p>
    ),
  },
];

const ContactPage: React.FC = () => {
  const [allList, setAllList] = useState([
    {
      name: '750 EAdama Dr.Tampa FL, 33619, USA',
    },
    {
      name: '750 EAdama Dr.Tampa FL, 33619, USA',
    },
    {
      name: '750 EAdama Dr.Tampa FL, 33619, USA',
    },
    {
      name: '750 EAdama Dr.Tampa FL, 33619, USA',
    },
    {
      name: '750 EAdama Dr.Tampa FL, 33619, USA',
    },
  ]);

  const [nearList, setNearList] = useState([]);

  return (
    <WrapConsultation className="wrap-consultation m-auto max-w-[1240px] px-10 pb-[40px] pt-[150px] max-md:px-5">
      <div>
        <div className="contact-us mb-[55px] flex max-md:flex-col-reverse max-md:flex-wrap">
          <div className="left w-1/2 pr-[70px] max-md:mt-[50px] max-md:w-full max-md:p-0">
            <div className="mb-[34px] text-[18px] font-semibold">
              Contact Us
            </div>
            <Input className="" placeholder="Your Name" />
            <Input className="" placeholder="Email id" />
            <Input className="" placeholder="Mobile number" />
            <Input.TextArea
              className="h-full "
              placeholder="Your Message"
              style={{
                height: '109px!important',
                resize: 'none',
              }}
            />
            <div>
              <button className="button-primary h-10 w-[100%]">SUBMIT</button>
            </div>
          </div>
          <div className="right w-1/2 max-md:w-full">
            <img
              className="m-auto mb-[27px]"
              src="/Isolation_Mode.png"
              alt=""
            />
            {contacts.map((item, index) => (
              <div key={index} className="mb-[16px] flex">
                <span className="icon pr-4">{item.icon}</span>
                <span>{item.text}</span>
              </div>
            ))}
            <div className="wrap-link-social">
              <div className="link-social">
                <div className="icon">
                  <FaceBookColorIcon />
                </div>
                <div className="icon">
                  <InstagramColorIcon />
                </div>
                <div className="icon">
                  <LinkInColorIcon />
                </div>
              </div>
              <div className="line"></div>
            </div>
          </div>
        </div>
      </div>
      <div>
        <div className="mb-5 text-[18px] font-semibold">Store near you</div>
        <div className="store-near-you flex max-md:flex-wrap">
          <div className="left w-3/5 max-md:w-full">
            {/* <GoogleMaps/> */}
            <MapsGoogle setNearList={setNearList} />
          </div>

          <div className="our-store-wrap w-2/5 pl-[40px] max-md:w-full">
            <h2 className="pb-4 text-[16px] max-md:mt-10">Our Stores</h2>
            {/* <div>there are {nearList.length} location near here</div> */}
            <ul className="ml-[19px]">
              {allList.map((item: any, index: number) => (
                <li key={index} className="mb-[30px]">
                  {item.name}
                </li>
              ))}
            </ul>
          </div>
        </div>
        <div className="q-and-a m-auto mt-[76px] flex gap-[25px] max-md:flex-col-reverse max-md:flex-wrap">
          <div className="right w-3/5 max-md:w-full">
            <CollapseBase items={items} />
          </div>
          <div className="left w-2/5 pt-[60px] max-md:w-full max-md:pt-0">
            <img
              className="m-auto w-[75%]"
              src="/images/finance/Frame.png"
              alt=""
            />
          </div>
        </div>
      </div>
    </WrapConsultation>
  );
};

export default ContactPage;
