'use client';

import styled from 'styled-components';

export const WrapConsultation = styled.div`
  .contact-us {
    .left {
      input {
        margin-bottom: 26px;
      }
      textarea {
        margin-bottom: 26px;
      }
    }
    .right {
      .wrap-link-social {
        display: flex;
        align-items: center;
        .link-social {
          display: flex;
          align-items: center;
          div.icon {
            margin-right: 23px;
          }
        }
        .line {
          flex-grow: 1;
          height: 1px;
          background-color: #969696;
        }
      }
    }
  }
  .q-and-a {
    .ant-collapse-header {
      padding-left: 0;
      padding-right: 0;
    }
  }
  .our-store-wrap{
    li{
      list-style: disc;
    }
  }
`;
