'use client';
import { WrapConsultation } from './style';

import { Select, Input, Radio, DatePicker } from 'antd';
import { useState } from 'react';

const images: any = {
  a: {
    src: '/images/service/house.png',
    price: '$49.00'
  },
  b: {
    src: '/images/our_services/HOTELS.jpeg',
    price: '$48.00'
  },
  c: {
    src: '/images/our_services/luxury-home.jpeg',
    price: '$47.00'
  },
  d: {
    src: '/images/our_services/APARTMENTS.jpg',
    price: '$46.00'
  }
}
const ConsultationPage: React.FC = () => {

  const [valueAppointment, setValueAppointment] = useState('a')

  const onChangeAppointmentType = (e: any) => {
    setValueAppointment(e.target.value)
  }
  return (
    <WrapConsultation className="wrap-consultation">
      <div className="m-auto max-w-screen-  px-10 pb-[100px] pt-[150px] max-md:px-5">
        <div className="relative m-auto flex max-w-[1200px] max-md:flex-wrap">
          <div className="left-side w-2/5 max-md:w-full">
            <div className="box-img max-md:!justify-center relative">
              <div className='absolute translate-x-2/4 font-semibold tracking-[1px] text-[21px] left-2/4 top-[21%]'>{images[valueAppointment].price}</div>
              <img src={images[valueAppointment].src} alt="" />
            </div>
          </div>

          <div className="right-side w-3/5 p-[35px] pt-5 max-md:w-full max-md:px-0">
            <div className="mb-5 flex gap-10 max-md:flex-wrap max-md:gap-5">
              <div className="w-1/2 max-md:w-full">
                <div className="mb-3">Select time</div>
                <Select
                  className="pr-[6px]"
                  defaultValue="time1"
                  options={[
                    { value: 'time1', label: 'time2' },
                    { value: 'lucy', label: 'Lucy' },
                  ]}
                />
              </div>

              <div className="w-1/2 max-md:w-full">
                <div className="mb-3">Choose Date</div>
                <DatePicker />
              </div>
            </div>
            <div className="mb-3">Select Appointment Type</div>
            <div className="mb-5">
              <Radio.Group
                defaultValue="a"
                size="large"
                className="!flex gap-10 max-md:gap-[12px] max-md:flex-wrap"
                onChange={(e: any) => onChangeAppointmentType(e)}
              >
                <div className="w-1/2 max-md:w-full">
                  <Radio.Button
                    value="a"
                    className="!mb-3 w-full !rounded-none !h-[48px] !flex items-center"
                  >
                    In Home Estimate
                  </Radio.Button>
                  <Radio.Button value="c" className="w-full !rounded-none !h-[48px] !flex items-center">
                    Virtual Call
                  </Radio.Button>
                </div>
                <div className="w-1/2 max-md:w-full">
                  <Radio.Button
                    value="b"
                    className="!mb-3 w-full !rounded-none !h-[48px] !flex items-center"
                  >
                    Phone Appointment
                  </Radio.Button>
                  <Radio.Button value="d" className="w-full !rounded-none !h-[48px] !flex items-center">
                    Store Appointment
                  </Radio.Button>
                </div>
              </Radio.Group>
            </div>
            <div>
              <div className="mb-3">Enter your details</div>
              <div className="detail flex gap-10 max-md:flex-wrap">
                <div className="left w-1/2 max-md:w-full max-md:!p-0">
                  <Input className="" placeholder="Your Name" />
                  <Input className="" placeholder="Your Email" />
                  <Input className="last" placeholder="Your Phone" />
                </div>
                <div className="right w-1/2 max-md:w-full max-md:!pl-0">
                  <Input.TextArea
                    className="!h-full !p-0"
                    placeholder="Your Message"
                    rows={4}
                    maxLength={6}
                  />
                </div>
              </div>
              <div className="mt-10 flex justify-end">
                <button className="button-primary h-10 w-[290px]">
                  SUBMIT
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </WrapConsultation>
  );
};

export default ConsultationPage;
