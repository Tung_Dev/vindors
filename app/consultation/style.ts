'use client';

import styled from 'styled-components';
import { primaryColor } from '../contants/css';
export const WrapConsultation = styled.div`
  .list-free {
    li {
      list-style: disc;
    }
  }
  .box-img {
    width: 100%;
    display: flex;
    justify-content: end;
    img {
      width: 85%;
    }
  }
  .ant-select-selector {
    border-radius: 0 !important;
  }
  .ant-select {
    width: 100%;
  }
  .right-side {
    .ant-select-selection-item {
      color: #d9d9d9;
    }
    .ant-select-selector,
    .ant-input {
      background-color: unset !important;
      align-items: center;
    }
    .detail {
      .left {
        padding-right: 6px;
        input {
          margin-bottom: 12px;
          &.last {
            margin-bottom: 0;
          }
        }
      }

      .right {
        padding-left: 6px;

        textarea {
          height: 100%;
        }
      }
    }
  }
  .ant-radio-button-wrapper {
    &:hover {
      color: ${primaryColor}!important;
    }
  }
  .ant-radio-button-wrapper-checked {
    color: ${primaryColor}!important;
    background-color: #e8feff !important;
    border: 1px solid ${primaryColor}!important;
  }
`;
