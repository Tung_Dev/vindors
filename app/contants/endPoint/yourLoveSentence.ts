const BASE_URI_USER = 'user'

export default {
    BASE_URI_USER,
    LOG_IN: `${BASE_URI_USER}/login`,
}