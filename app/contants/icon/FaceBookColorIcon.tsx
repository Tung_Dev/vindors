import React from 'react';

const FaceBookColorIcon: React.FC = () => (
  <i>
    <svg width="39" height="39" viewBox="0 0 39 39" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="19.475" cy="19.475" r="19.475" fill="#4167B1" />
      <path d="M20.4905 27.3476V19.9499H22.9726L23.345 17.066H20.4905V15.225C20.4905 14.3904 20.7214 13.8215 21.9197 13.8215L23.4455 13.8209V11.2414C23.1816 11.2072 22.2758 11.1285 21.2216 11.1285C19.0202 11.1285 17.5131 12.4723 17.5131 14.9394V17.066H15.0234V19.9499H17.5131V27.3476H20.4905Z" fill="white" />
    </svg>


  </i>
);

export default FaceBookColorIcon;
