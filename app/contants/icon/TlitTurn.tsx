import React from 'react';

const TlitTurn: React.FC = () => (
<i>
  <svg
    xmlns="http://www.w3.org/2000/svg"
    xmlnsXlink="http://www.w3.org/1999/xlink"
    x="0px"
    y="0px"
    viewBox="0 0 52 65"
    xmlSpace="preserve"
  >
    <g transform="translate(30 31)">
      <g>
        <defs>
          <rect x={-30} y={-31} width={52} height={65} />
        </defs>
        <clipPath>
          
          <use xlinkHref="#SVGID_3_" style={{ overflow: "visible" }} />
        </clipPath>
        <g className="WDT0">
          
          <path
            className="WDT1"
            d="M20.7,33.6H-29c-0.6,0-1-0.5-1-1V-30c0-0.6,0.4-1,1-1h49.7c0.6,0,1,0.5,1,1v62.5    C21.7,33.1,21.3,33.6,20.7,33.6 M-28,31.5h47.7v-60.4H-28L-28,31.5z"
          />
          <path
            className="WDT1"
            d="M15.7,28.4H-24c-0.6,0-1-0.5-1-1v-52.1c0-0.6,0.4-1,1-1h39.8c0.6,0,1,0.5,1,1v52.1    C16.7,27.9,16.3,28.4,15.7,28.4 M-23,26.3h37.8v-50H-23V26.3z"
          />
          <path
            className="WDT1"
            d="M-15.1-5c-0.2,0-0.4-0.1-0.6-0.2c-0.5-0.3-0.6-1-0.3-1.4l6-9.4c0.3-0.5,0.9-0.6,1.3-0.3    c0,0,0,0,0.1,0c0.5,0.3,0.6,1,0.3,1.4l-6,9.4C-14.4-5.1-14.8-4.9-15.1-5"
          />
          <path
            className="WDT1"
            d="M-10.1,1.3c-0.2,0-0.4-0.1-0.6-0.2c-0.5-0.3-0.6-1-0.3-1.4l4-6.3C-6.7-7-6.1-7.2-5.6-6.9    c0,0,0,0,0.1,0c0.5,0.3,0.6,1,0.3,1.4l-4,6.3C-9.5,1.1-9.8,1.3-10.1,1.3"
          />
          <path
            className="WDT1"
            d="M-2.2-11.2c-0.2,0-0.4-0.1-0.6-0.2c-0.5-0.3-0.6-1-0.3-1.4l1.3-2.1c0.3-0.5,0.9-0.6,1.3-0.3    c0,0,0,0,0.1,0c0.5,0.3,0.6,1,0.3,1.4l-1.3,2.1C-1.5-11.4-1.8-11.2-2.2-11.2"
          />
        </g>
      </g>
    </g>
  </svg>
</i>

);

export default TlitTurn;
