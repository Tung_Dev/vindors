import React from 'react';

const Arrow: React.FC = () => (
  <i>
    <svg
      xmlns="http://www.w3.org/2000/svg"
      xlinkHref="http://www.w3.org/1999/xlink"
      x="0px"
      y="0px"
      viewBox="0 0 60 95"
      xmlSpace="preserve"
      style={{
        height: '100px',
        width: '90px',
      }}
    >
      <g>
        <g>
          <rect
            x="29.5"
            y="-6.5"
            transform="matrix(0.7071 -0.7071 0.7071 0.7071 -9.1254 28.6329)"
            width="0.9"
            height="63.6"
          ></rect>
        </g>
        <g>
          <rect
            x="-1.8"
            y="69.2"
            transform="matrix(0.7071 -0.7071 0.7071 0.7071 -40.4763 41.6186)"
            width="63.6"
            height="0.9"
          ></rect>
        </g>
      </g>
    </svg>
  </i>
);

export default Arrow;
