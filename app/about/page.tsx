'use client';
import Image from 'next/image';

import { useState, useRef, useEffect } from 'react';
import { WrapAbout } from '#/app/about/style';
import { Modal } from 'antd';
import {
  ArrowRightOutlined,
  CaretRightOutlined,
  StarFilled,
} from '@ant-design/icons';
import StairImpactIcon from '#/app/contants/icon/StairImpactIcon';
import WindowImpactIcon from '#/app/contants/icon/WindowImpactIcon';
import DoorImpactIcon from '#/app/contants/icon/DoorImpactIcon';
import UIImageUpDownSide from '#/app/UI/UIImageUpDownSide';
import UIWhyWeAreBest from '#/app/UI/UIWhyWeAreBest';

// const imageCards = [
//   {
//     src: 'https://vindors.wpengine.com/wp-content/uploads/2023/04/service-1.jpg',
//     text: 'DOOR FITTING',
//   },
//   {
//     src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/service-home-image-4.webp',
//     text: 'STEEL GARAGE DOORS',
//   },

//   {
//     src: 'https://vindors.wpengine.com/wp-content/uploads/2023/04/service-2.webp',
//     text: 'WOODEN FRAME WINDOWS',
//   },
//   {
//     src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/service-home-image-5.webp',
//     text: 'NATURAL FINISH WINDOWS',
//   },

//   {
//     src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/service-home-image-3.webp',
//     text: 'IRON FRAME DOORS',
//   },

//   {
//     src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/service-home-image6.webp',
//     text: 'GLASS PANEL WINDOWS',
//   },
// ];
const iconMainProduct = [
  {
    icon: <StairImpactIcon />,
    name: 'START',
    link: '',
  },
  {
    icon: <WindowImpactIcon />,
    name: 'WINDOW',
    link: '',
  },
  {
    icon: <DoorImpactIcon />,
    name: 'DOOR',
    link: '',
  },
];

const imagesTeam = [
  {
    src: '/images/about/1.png',
    name: 'Project manager',
  },
  {
    src: '/images/about/2.png',
    name: 'Assistant Manager',
  },
  {
    src: '/images/about/3.png',
    name: 'Marketing Head',
  },
  {
    src: '/images/about/4.png',
    name: 'Architecture',
  },
  {
    src: '/images/about/3.png',
    name: 'Marketing Head',
  },
  {
    src: '/images/about/4.png',
    name: 'Architecture',
  },
  {
    src: '/images/about/2.png',
    name: 'Assistant Manager',
  },
  {
    src: '/images/about/1.png',
    name: 'Project manager',
  },
];

const AboutPage: React.FC = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <WrapAbout className="wrap-about m-auto max-w-[1240px] px-[30px]">
      {/* <Banner></Banner> */}

      <div className="m-auto mb-[180px] flex max-w-[1640px] px-[20px] pt-[200px] max-md:mb-[70px] max-md:flex-wrap max-md:pt-[100px]">
        <div className="w-1/2 max-md:w-full">
          <UIImageUpDownSide />
        </div>
        <div className="w-1/2 py-0 pl-[35px] pr-5 max-md:mt-[110px] max-md:w-full">
          <h1 className="big-text mb-5">ABOUT US</h1>
          {/* <UIcounting
            title="ALUMINUM WINDOWS & DOORS REPAIR SPECIALIST"
            count1={{
              num: '2,880',
              text: 'Ullamcorper morbi tincidunt ornare massa eget.',
            }}
            count2={{
              num: '560',
              text: 'Aenean vel elit scelerisque mauris pellentesque.',
            }}
            textButton="SHOP NOW"
          /> */}
          <p className="font-light tracking-[1px]">
            20 YEARS IN THE INDUSTRY Based in Florida, we have a rich legacy and
            20 years of experience in the construction industry. We’ve changed
            barren spaces into eye-catching structures; keeping up with the
            trends & evolving with technology as we go. Our journey began with a
            vision to transform the way people perceive and experience windows
            and doors. With a drive for innovation, safety and craftsmanship we
            have grown to become a trusted name in the industry accross the US
            and beyond. <br /> <br /> We have worked with hotels, hospitals,
            houses, commercial spaces & multi-family homes. Now we have
            customers accross the globe and hundreds of difference designs, but
            our expansion does not mean a loss of quality, but an acceleration
            to provide top-quality windows and doors that not only enhance the
            aesthetics of your space but also offer superior functionality,
            security, and energy efficiency.
          </p>
          <div className="mt-[40px]">
            <button className="button-primary h-10 w-[288px]">READ ALL</button>
          </div>
        </div>
      </div>

      {/* <div className="flex justify-around pb-[120px] pl-5 pr-5">
        {iconMainProduct.map((item, index) => (
          <UIIconViewMore
            key={index}
            icon={item.icon}
            name={item.name}
            link={item.link}
          />
        ))}
      </div> */}

      <div className="relative  px-10 pb-[120px] max-md:px-5">
        <div className="m-auto max-w-[1440px]">
          {/* <div
            className="background-overlay"
            style={{
              backgroundImage:
                "url('https://vindors.wpengine.com/wp-content/uploads/2023/03/AdobeStock_198081475.png')",
            }}
          ></div> */}

          <h5 className="big-text pb-[30px] text-center">OUR HISTORY</h5>
          <span className="m-auto mb-[30px] block text-center font-light tracking-[1px]">
            20 YEARS IN THE INDUSTRY Based in Florida, we have a rich legacy and
            20 years of experience in the construction industry. We’ve changed
            barren spaces into eye-catching structures; keeping up with the
            trends & evolving with technology as we go.
          </span>
          <div className="relative">
            <span className="play-trigger" onClick={showModal}>
              <CaretRightOutlined rev={undefined} />
            </span>
            <Modal
              open={isModalOpen}
              onOk={handleOk}
              onCancel={handleCancel}
              footer={null}
              className="model-iframe"
            >
              <iframe
                width="560"
                height="315"
                src="https://www.youtube.com/embed/13BLN2qh6sw?si=aPaWB6w3LijlpqGJ"
                title="YouTube video player"
                frameBorder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                allowFullScreen
              ></iframe>
            </Modal>
            <Image
              src="/AdobeStock_564386994-1.jpeg"
              alt="AdobeStock"
              width="1440"
              height="500"
            />
          </div>

          {/* <h3 className="text-description-about">
            Sed molestie ex sem, vel sodales felis rhoncus volutpat. Fusce diam
            justo, tempus sed rhoncus non, ac, lobortis aliquam nibh. Mauris
            risus, nec faucibus urna. Phasellus. Nullam ilisis lobortis
            vulputate. Nam malesuada ligula non magna tincidunt sodales. Morbi
            gravida auctor nisl, eimentu.
          </h3> */}
          {/* <div className="m-auto mb-[50px] w-fit">
            <Signature />
            <span>WALTER WHITE - CEO</span>
          </div> */}
          {/* <button className="button-primary m-auto h-10 w-[288px]">
            <a href="/about" key="/about">
              KNOW MORE ABOUT US
            </a>
          </button> */}
        </div>
      </div>

      {/* <div className="pb-[60px] pl-5 pr-5">
        <div className="big-text pb-[30px] text-center">WHY WE ARE BEST</div>
        <UIWhyWeAreBest />
      </div> */}
      <div className="pb-[60px] pl-5 pr-5">
        <div className="big-text text-center">MEET THE TEAM</div>
        <p className="medium-text m-auto pb-[75px] pt-[18px] text-center uppercase">
          Lorem Ipsum is simply dummy text of the printin and
        </p>

        <div className="flex flex-wrap">
          {imagesTeam.map((item, index) => (
            <div
              className="w-1/4 px-[30px] py-5 max-md:w-1/2 max-sm:w-full"
              key={index}
            >
              <div className="bg-[#fafafa]">
                <img src={item.src} alt="" />
                <h2 className="medium-text pb-[10px] pt-[29px] text-center font-semibold uppercase">
                  {item.name}
                </h2>
                <h3 className="pb-[10px] text-center">Project Manager</h3>
                <a href="">
                  <div className="center gap-1 pb-[10px] text-center text-[12px]">
                    View Details{' '}
                    <i>
                      <svg
                        width="15"
                        height="8"
                        viewBox="0 0 15 8"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M14.3536 4.35355C14.5488 4.15829 14.5488 3.84171 14.3536 3.64645L11.1716 0.464467C10.9763 0.269205 10.6597 0.269205 10.4645 0.464467C10.2692 0.659729 10.2692 0.976312 10.4645 1.17157L13.2929 4L10.4645 6.82843C10.2692 7.02369 10.2692 7.34027 10.4645 7.53553C10.6597 7.7308 10.9763 7.7308 11.1716 7.53553L14.3536 4.35355ZM-4.37114e-08 4.5L14 4.5L14 3.5L4.37114e-08 3.5L-4.37114e-08 4.5Z"
                          fill="#434343"
                        />
                      </svg>
                    </i>
                  </div>
                </a>
              </div>
            </div>
          ))}
        </div>
      </div>

      {/* <UIInstagram className="mb-[30px]" /> */}

      {/* <div className="m-auto mt-96 max-w-screen-2xl">
        <div className="image-card-section flex">
          <div className="image-card-section-item pl-5 pr-5">
            {imageCards.slice(0, 2).map((item, index) => (
              <ImageCardHover key={index} src={item.src} text={item.text} />
            ))}
          </div>
          <div className="image-card-section-item pl-5 pr-5">
            {imageCards.slice(2, 4).map((item, index) => (
              <ImageCardHover key={index} src={item.src} text={item.text} />
            ))}
          </div>
          <div className="image-card-section-item pl-5 pr-5">
            {imageCards.slice(4, 6).map((item, index) => (
              <ImageCardHover key={index} src={item.src} text={item.text} />
            ))}
          </div>
        </div>
      </div> */}
    </WrapAbout>
  );
};

export default AboutPage;
