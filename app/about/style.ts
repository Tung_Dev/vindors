

import styled from 'styled-components';

export const WrapAbout = styled.div`
@keyframes shadowPulse {
  0% {
    box-shadow: 0 0 0 0 rgba(255,190,11, 0.4);
  }
  50% {
    box-shadow: 0 0 0 40px rgba(255,190,11, 0);
  }
  100% {
    box-shadow: 0 0 0 0 rgba(255,190,11, 0);
  }
}
.play-trigger{
    grid-area: 1/-1;
    position: absolute;
    top: 50%;
    display: inline-flex;
    align-items: center;
    justify-content: center;
    font-size: 12px;
    letter-spacing: 2.64px;
    text-transform: uppercase;
    font-weight: 600;
    width: clamp(5rem, 4.2788rem + 3.2051vw, 8.125rem);
    height: clamp(5rem, 4.2788rem + 3.2051vw, 8.125rem);
    border-radius: 50%;
    backdrop-filter: blur(3px) brightness(0.8);
    -webkit-backdrop-filter: blur(3px) brightness(0.8);
    /* box-shadow: 0 0 0 2px rgb(255,190,11, 1); */
    transition: all 0.3s linear 0s;
    cursor: pointer;
    transform: translate(-50%, -50%);
    left: 50%;
    color: #FFFFFF;
    /* &:hover{
        animation: shadowPulse 1.3s linear 1;
    } */
    svg{
      width: 40px;
    height: 40px;
    }
}

.text-description-about {
    letter-spacing: 2px;
    font-weight: 500;
    font-size: 22px;
    text-align: center;
    margin: 50px 0;
}

`;
