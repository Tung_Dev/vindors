'use client';
import SlickReact from '#/app/components/SlickReact';
import DoorImpactIcon from '#/app/contants/icon/DoorImpactIcon';
import HungWindow from '#/app/contants/icon/HungWindow';
import SkyLightWindow from '#/app/contants/icon/SkyLightWindow';
import SlidingWindow from '#/app/contants/icon/SlidingWindow';
import TlitTurn from '#/app/contants/icon/TlitTurn';
import TransomWindow from '#/app/contants/icon/TransomWindow';
import { StarFilled } from '@ant-design/icons';
import React, { useState } from 'react';
import { WrapUIWhatCustomerSay } from './style';

const reviewRatings = [
  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/home-testimonial-1.webp',
    name: 'Ema Thomas',
    position: 'CEO',
    rating: 5,
    comment:
      `Absolutely thrilled with the windows and doors from doitimpact. The craftsmanship is impeccable, and they've transformed the look of my home. The team was professional and the installation was seamless. Highly recommended.`,
  },
  {
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/home-testimonial-2.webp',
    name: 'Diya George',
    position: 'Manager',
    rating: 5,
    comment:
      `I can't say enough good things about doitimpact. The staircase they designed for us is not only functional but also a work of art. It became a focal point in our home. Their attention to detail and customer services are second to none. Very satisfied!`,
  },
  // {
  //   src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/home-testimonial-3.webp',
  //   name: 'Willium',
  //   position: 'Customer Support',
  //   rating: 5,
  //   comment:
  //     'Tellus in hac habitasse platea dictumst vestibulum rhoncus est pellentesque. Faucibus purus in tempor nec. Placerat orci nulla pellentesque orci dignissim enim sit.',
  // },
];

function SampleNextArrow(props: any) {
  const { className, style, onClick, currentSlide } = props;
  return (
    <div className={className} style={{ ...style }} onClick={onClick}></div>
  );
}

function SamplePrevArrow(props: any) {
  const { className, style, onClick, currentSlide } = props;
  return (
    <div className={className} style={{ ...style }} onClick={onClick}></div>
  );
}

interface UIWhatCustomerSayProps {
  className?: string;
}

const bestArr = [
  {
    icon: <DoorImpactIcon />,
    title: 'ENTRANCE DOORS',
    desc: 'Viverra ipsum nunc aliquet bibendum enim facilisis volutpat blandit aliquam etiam erat velit scelerisque viverra mauris in aliquam.',
  },
  {
    icon: <TlitTurn />,
    title: 'TILT & TURN',
    desc: 'Lobortis mattis aliquam faucibus purus eget nullam non nisi est ipsum faucibus vitae aliquet nec ullamcorper sit amet.',
  },
  {
    icon: <SlidingWindow />,
    title: 'SLIDING WINDOWS',
    desc: 'Dolor sed viverra ipsum nunc aliquet bibendum enim facilisis gravida sagittis purus sit amet volutpat consequat massa.',
  },
  {
    icon: <HungWindow />,
    title: 'HUNG WINDOW',
    desc: 'Dolor sed viverra ipsum nunc aliquet bibendum enim facilisis gravida sagittis purus sit amet volutpat consequat massa.',
  },
  {
    icon: <SkyLightWindow />,
    title: 'SKYLIGHT WINDOW',
    desc: 'Dolor sed viverra ipsum nunc aliquet bibendum enim facilisis gravida sagittis purus sit amet volutpat consequat massa.',
  },
  {
    icon: <TransomWindow />,
    title: 'TRANSOM WINDOW',
    desc: 'Cras sed felis eget velit aliquet sagittis id consectetur purus semper feugiat nibh sed pulvinar proin gravida cum sociis.',
  },
];

const UIWhatCustomerSay: React.FC<UIWhatCustomerSayProps> = ({
  className = '',
}) => {
  const [slider1, setSlider1] = useState(null);
  const [slider2, setSlider2] = useState(null);
  return (
    <WrapUIWhatCustomerSay className={`${className} relative`}>
      <div
        className="background-overlay"
        style={{
          opacity: '0.3',
          backgroundImage:
            "url('https://vindors.wpengine.com/wp-content/uploads/2023/03/Vin-Testimonial-Overlay-1.png')",
        }}
      ></div>
      <div className="quote-left">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          xmlnsXlink="http://www.w3.org/1999/xlink"
          x="0px"
          y="0px"
          viewBox="0 0 67 45"
          xmlSpace="preserve"
        >
          <g>
            <path d="M1.9,4.3v21.7c0,1.6,1.3,2.8,2.8,2.8h6.8c1.5,0,2.6,1.2,2.6,2.6v9.3c0,1.5,1.2,2.6,2.6,2.6h0.7  c0.8,0,1.6-0.4,2.1-1.1L28,30.8c0.9-1.2,1.4-2.7,1.4-4.2V4.3c0-1.6-1.3-2.8-2.8-2.8H4.8C3.2,1.5,1.9,2.8,1.9,4.3"></path>
            <path d="M37.6,4.3v21.7c0,1.6,1.3,2.8,2.8,2.8h6.8c1.5,0,2.6,1.2,2.6,2.6v9.3c0,1.5,1.2,2.6,2.6,2.6h0.7  c0.8,0,1.6-0.4,2.1-1.1l8.3-11.6c0.9-1.2,1.4-2.7,1.4-4.2V4.3c0-1.6-1.3-2.8-2.8-2.8H40.5C38.9,1.5,37.6,2.8,37.6,4.3"></path>
          </g>
        </svg>
      </div>
      <div className="quote-right">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          xmlnsXlink="http://www.w3.org/1999/xlink"
          x="0px"
          y="0px"
          viewBox="0 0 67 45"
          xmlSpace="preserve"
        >
          <g>
            {' '}
            <path d="M1.9,4.3v21.7c0,1.6,1.3,2.8,2.8,2.8h6.8c1.5,0,2.6,1.2,2.6,2.6v9.3c0,1.5,1.2,2.6,2.6,2.6h0.7  c0.8,0,1.6-0.4,2.1-1.1L28,30.8c0.9-1.2,1.4-2.7,1.4-4.2V4.3c0-1.6-1.3-2.8-2.8-2.8H4.8C3.2,1.5,1.9,2.8,1.9,4.3"></path>
            <path d="M37.6,4.3v21.7c0,1.6,1.3,2.8,2.8,2.8h6.8c1.5,0,2.6,1.2,2.6,2.6v9.3c0,1.5,1.2,2.6,2.6,2.6h0.7  c0.8,0,1.6-0.4,2.1-1.1l8.3-11.6c0.9-1.2,1.4-2.7,1.4-4.2V4.3c0-1.6-1.3-2.8-2.8-2.8H40.5C38.9,1.5,37.6,2.8,37.6,4.3"></path>
          </g>
        </svg>
      </div>

      <div className="big-text m-auto pb-[40px] text-center px-5">
        WHAT CUSTOMERS SAYS
      </div>
      <SlickReact
        className="slide-1 max-md:px-5"
        settings={{
          centerPadding: '60px',
          asNavFor: slider2,
          ref: (slider: any) => setSlider1(slider),
          slidesToShow: 1,
          swipeToSlide: true,
          focusOnSelect: true,
        }}
      >
        {reviewRatings.map((item, index) => (
          <div key={index} className="text-comment">
            &quot; {item.comment} &quot;
          </div>
        ))}
      </SlickReact>
      <SlickReact
        className="slide-2"
        settings={{
          className: 'center',
          centerPadding: '60px',
          asNavFor: slider1,
          ref: (slider: any) => setSlider2(slider),
          slidesToShow: 1,
          swipeToSlide: true,
          // fade: true,
          nextArrow: <SampleNextArrow />,
          prevArrow: <SamplePrevArrow />,
        }}
      >
        {reviewRatings.map((item, index) => (
          <div key={index} className="wrap-image">
            <div className="prev-img">
              <img
                src={
                  index === 0
                    ? reviewRatings[reviewRatings.length - 1].src
                    : reviewRatings[index - 1].src
                }
                alt=""
              />
            </div>
            <div className="main-user">
              <img src={item.src} alt="" />
              <div className="rating">
                <div className="mb-3">
                  <StarFilled rev={undefined} />
                  <StarFilled rev={undefined} />
                  <StarFilled rev={undefined} />
                  <StarFilled rev={undefined} />
                  <StarFilled rev={undefined} />
                </div>
                <h5 className="user">
                  {item.name} - {item.position}
                </h5>
              </div>
            </div>

            <div className="next-img">
              <img
                src={
                  index === reviewRatings.length - 1
                    ? reviewRatings[0].src
                    : reviewRatings[index + 1].src
                }
                alt=""
              />
            </div>
          </div>
        ))}
      </SlickReact>
    </WrapUIWhatCustomerSay>
  );
};

export default UIWhatCustomerSay;
