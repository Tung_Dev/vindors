'use client';

import styled from 'styled-components';

export const WrapUIWhatCustomerSay = styled.div`
  .wrap-image {
    display: flex !important;
    position: relative;
    justify-content: center;
    align-items: center;
    text-align: center;
    height: 400px;
    .prev-img {
      top: 160px;
      transform: translate(50%, -50%);
    }
    .next-img {
      top: 160px;
      transform: translate(-50%, -50%);
    }
  }

  /* } */
  .slick-slide {
    img {
      margin: auto;
      border: 2px solid #ffffff;
      max-width: 80px;
      border-radius: 50%;
    }
    .main-user {
      width: 245px;
      height: 250px;
      display: flex;
      flex-direction: column;
      justify-content: center;
      align-items: center;
      img {
        transform: scale(1.6);
        transition: all 0.3s ease;
      }
      .rating {
        svg {
          fill: #ffbe0b;
        }
      }
      .user {
        text-transform: uppercase;
        letter-spacing: 3px;
        font-weight: 300;
      }
    }
  }
  .slide-1 {
    .slick-slide {
      text-align: center;
    }
    .text-comment {
      max-width: 850px;
      font-size: 22px;
      font-style: italic;
      font-weight: 300;
      letter-spacing: 2px;
    }
    .slick-arrow {
      display: none !important;
    }
  }
  .slide-2 {
    .slick-arrow {
      width: 80px;
      height: 80px;
      border-radius: 50%;

      &::before {
        content: none;
      }
      &.slick-prev {
        top: 50%;
        right: 50%;
        transform: translate(-163px, -80px);
      }
      &.slick-next {
        top: 50%;
        right: 50%;
        transform: translate(164px, -80px);
      }
    }
  }

  .quote-right {
    position: absolute;
    top: 50%;
    right: 4%;
    svg {
      width: 60px;
      height: 60px;
      fill: #00000010;
    }
  }
  .quote-left {
    position: absolute;
    top: 50%;
    left: 4%;
    transform: rotateY(180deg);
    svg {
      width: 60px;
      height: 60px;
      fill: #00000010;
    }
  }
`;
