'use client';

import styled from 'styled-components';

export const WrapUIcounting = styled.div`
.wrap-count {
    /* max-width: 316px; */
    display: flex;
    align-items: start;
    flex-direction: column;
    .number-count {
      &:hover {
        &:before {
          width: 100%;
          transform: translateX(-32px);
        }
        &:after {
          height: 0%;
        }
      }
      &:after {
        content: '';
        position: absolute;
        width: 100%;
        height: 100%;
        border: 1px solid #8e8e8e;
        border-width: 0 2px 0 0;
        right: 0;
        bottom: 0;
        top: 0;
        margin: auto;
        transition: all 0.5s ease;
        -webkit-transition: all 0.5s ease;
      }
      &:before {
        content: '';
        position: absolute;
        width: 100px;
        height: 100%;
        border: 1px solid #8e8e8e;
        border-width: 2px 0 2px 0;
        right: 0;
        bottom: 0;
        top: 0;
        margin: auto;
        transition: all 0.5s ease;
      }
    }
  }
`;
