'use client';
import React from 'react';
import { WrapUIcounting } from './style';
interface UIcountingProps {
  count1?: any;
  count2?: any;
  title?: string;
  textButton?: string;
  className?: string;
}

const UIcounting: React.FC<UIcountingProps> = ({
  count1,
  count2,
  title,
  textButton,
  className = '',
}) => (
  <WrapUIcounting className={className}>
    <div className=" pl-[100px]">
      <div className="big-text pt-20">{title}</div>

      <div className="flex space-x-4 pb-10 pt-[25px] max-md:flex-wrap">
        <div className="wrap-count mr-2 max-md:w-full lg:w-1/2">
          <div className="number-count relative mb-5 mt-5 pr-8 text-8xl font-black">
            {count1.num}
          </div>
          <span>{count1.text}</span>
        </div>

        <div className="wrap-count ml-2 max-md:w-full lg:w-1/2">
          <div className="number-count relative mb-5 mt-5 pr-8 text-8xl font-black">
            {count2.num}
          </div>
          <span>{count2.text}</span>
        </div>
      </div>
      <div>
        <button className="button-primary h-10 w-[288px]">
          <a href="/about" key="/about">
            {textButton}
          </a>
        </button>
      </div>
    </div>
  </WrapUIcounting>
);

export default UIcounting;
