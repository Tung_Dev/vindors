'use client';
import CardHoverZoomIn from '#/app/components/CardHoverZoomIn';
import { InstagramOutlined } from '@ant-design/icons';
import React, { useEffect, useState } from 'react';
import { WrapUIInstagram } from './style';
import SlickReact from '#/app/components/SlickReact';
// import { isMobile, isTablet } from 'react-device-detect';
interface UIInstagramProps {
  className?: string;
}

const imageCardHoverZoomIn = [
  {
    img: 'https://vindors.wpengine.com/wp-content/uploads/2023/03/insta-1.jpg',
    icon: <InstagramOutlined rev={undefined} />,
  },
  {
    img: 'https://vindors.wpengine.com/wp-content/uploads/2023/03/insta-2.jpg',
    icon: <InstagramOutlined rev={undefined} />,
  },
  {
    img: 'https://vindors.wpengine.com/wp-content/uploads/2023/03/insta-3.jpg',
    icon: <InstagramOutlined rev={undefined} />,
  },
  {
    img: 'https://vindors.wpengine.com/wp-content/uploads/2023/03/insta-4.jpg',
    icon: <InstagramOutlined rev={undefined} />,
  },
  {
    img: 'https://vindors.wpengine.com/wp-content/uploads/2023/03/insta-5.jpg',
    icon: <InstagramOutlined rev={undefined} />,
  },
  {
    img: 'https://vindors.wpengine.com/wp-content/uploads/2023/03/insta-8.jpg',
    icon: <InstagramOutlined rev={undefined} />,
  },
  {
    img: 'https://vindors.wpengine.com/wp-content/uploads/2023/03/insta-2.jpg',
    icon: <InstagramOutlined rev={undefined} />,
  },
  {
    img: 'https://vindors.wpengine.com/wp-content/uploads/2023/03/insta-6.jpg',
    icon: <InstagramOutlined rev={undefined} />,
  },
  {
    img: 'https://vindors.wpengine.com/wp-content/uploads/2023/03/insta-7.jpg',
    icon: <InstagramOutlined rev={undefined} />,
  },
  {
    img: 'https://vindors.wpengine.com/wp-content/uploads/2023/03/insta-8.jpg',
    icon: <InstagramOutlined rev={undefined} />,
  },
];

const UIInstagram: React.FC<UIInstagramProps> = ({ className = '' }) => {
  return (
    <WrapUIInstagram className={`${className} px-[20px]`}>
      <div className="big-text m-auto pt-[80px] text-center">
        INSTAGRAM @ DOITIMPACT
      </div>
      <SlickReact
        settings={{
          cssEase: 'linear',
          arrows: false,
        }}
        total={imageCardHoverZoomIn.length}
        checkWidthToShow
        className="translate-y-[20%]"
      >
        {imageCardHoverZoomIn.map((item, index) => (
          <CardHoverZoomIn
            key={index}
            src={item.img}
            icon={item.icon}
            className="w-full"
          />
        ))}
      </SlickReact>
    </WrapUIInstagram>
  );
};

export default UIInstagram;
