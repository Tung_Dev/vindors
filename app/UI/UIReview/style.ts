import styled from 'styled-components';
import { primaryColor } from '../../contants/css';

export const WrapUIReview = styled.div`
  @keyframes front {
    0% {
      left: 0%;
      display: block;
      width: 101%;
    }
    100% {
      left: 100%;
      display: none;
      width: 1%;
    }
  }
  @keyframes back {
    0% {
      left: 0%;
      display: block;
    }
    100% {
      left: -100%;
      display: none;
    }
  }
  .slick-slide.slick-active.slick-current {
    position: relative;
    .image-slider {
      position: absolute;
      width: 100%;
      height: 100%;
      background-color: #ffffff;
    }
    .image-slider.back {
      animation: back 0.5s ease-in forwards;
    }
    .image-slider.front {
      animation: front 0.5s ease-in forwards;
    }
  }
  .slick-arrow.slick-next {
    width: 100px;
    height: fit-content;
    transform: translateX(-80px);
    &::before {
      content: unset;
    }
    &:hover {
      svg {
        fill: ${primaryColor};
      }
      .next-text {
        color: ${primaryColor};
      }
    }
    .next-text {
      font-size: 12px;
      font-weight: 500;
      text-transform: uppercase;
      letter-spacing: 2px;
      color: #000000;
      position: absolute;
      top: 50%;
      transform: translate(0, -50%);
    }
    svg {
      height: 100px;
      width: 90px;
    }
  }

  .rating {
    svg {
      width: 13.5px;
      height: 13px;
      fill: #ffbe0b;
    }
    .user {
      font-size: 12px;
      font-weight: 500;
      text-transform: uppercase;
      letter-spacing: 3px;
      color: #0060ff;
    }
  }
  @media only screen and (max-width: 1024px) {
    .slick-arrow.slick-next {
      transform: translate(-8px, -267%);
    }
  }
`;
