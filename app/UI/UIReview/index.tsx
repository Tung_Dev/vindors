'use client';
import React, { useState, useEffect, useRef, Fragment } from 'react';
import Slider from 'react-slick';

import { WrapUIReview } from './style';

import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import Arrow from '#/app/contants/icon/Arrow';
import { StarFilled, StarOutlined } from '@ant-design/icons';

function SampleNextArrow(props: any) {
  const { className, style, onClick } = props;
  return (
    <div className={className} onClick={onClick}>
      <span className="next-text">NEXT</span>
      <Arrow />
    </div>
  );
}

const arrItems = [
  {
    comment:
      'Aliquam sed nisi eu eros tristique scelerisque. Aliquam eu tellus quis lacus luctus cursus. Aenean dapibus lacus vitae lobortis aliquam. Cras susci eros lacus, condimentum porta odio hendrerit vel. Sed ultricesque.',
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/03/Vin-Testimonal-type-2-bg-2.png',
  },
  {
    comment:
      'Cras susci eros lacus, condimentum porta odio hendrerit vel. Sed ultricesque. Aliquam sed nisi eu eros tristique scelerisque. Aliquam eu tellus quis lacus luctus cursus. Aenean dapibus lacus vitae lobortis aliquam.',
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/home-3-testimonial-1.jpg',
  },
  {
    comment:
      'Cras susci eros lacus, condimentum porta odio hendrerit vel. Sed ultricesque. Aliquam sed nisi eu eros tristique scelerisque. Aliquam eu tellus quis lacus luctus cursus. Aenean dapibus lacus vitae lobortis aliquam.',
    src: 'https://vindors.wpengine.com/wp-content/uploads/2023/05/home-3-testimonial-2.jpg',
  },
];

const UIReview: React.FC = () => {
  const [sliderRef, setSliderRef] = useState<any | null>({});
  const [isDragging, setIsDragging] = useState(false);

  const settings = {
    infinite: true,
    fade: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 500,
    beforeChange: (prev: number, next: number) => { },
    nextArrow: <SampleNextArrow />,
  };

  return (
    <WrapUIReview>
      <Slider {...settings}>
        {arrItems.map((item, index) => (
          <div key={index}>
            <div className="flex max-lg:flex-wrap">
              <div className="relative relative mr-[80px] flex w-5/12 max-w-[570px] items-center max-lg:w-full">
                <div className="w-full opacity-20">
                  <div className="image-slider back"></div>
                  <img src={item.src} alt="" />
                </div>
                <div className="absolute ml-[140px] translate-x-[17%]">
                  <div className="image-slider front"></div>
                  <img className="image-main" src={item.src} alt="" />
                </div>
              </div>

              <div className="mb-[50px] ml-[80px] flex w-6/12 items-center text-[22px] font-medium italic max-lg:w-full max-lg:pt-[30px]">
                <div>
                  <p className="mb-[50px]">&ldquo;{item.comment}&rdquo;</p>
                  <div className="rating">
                    <div className="mb-3">
                      <StarFilled rev={undefined} />
                      <StarFilled rev={undefined} />
                      <StarFilled rev={undefined} />
                      <StarFilled rev={undefined} />
                      <StarFilled rev={undefined} />
                    </div>
                    <h5 className="user ">CRYSTOPHER - CEO</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        ))}
      </Slider>
    </WrapUIReview>
  );
};

export default UIReview;
