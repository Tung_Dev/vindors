'use client';

import styled from 'styled-components';

export const WrapUIWhyWeAreBest = styled.div`
    display: flex;
    flex-wrap: wrap;
    .box-item {
        &:hover {
            box-shadow: 0px 0px 7px 1px rgba(217.50000000000003, 217.50000000000003, 217.50000000000003, 0.5);
            .number{
                opacity: 0.05;
            }
        }
        .number {
            position: absolute;
            font-size: 100px;
            top: -12px;
            left: 35px;
            font-weight: 600;
            z-index: -1;
            opacity: 0.02;
        }
        .icon {
            svg{
                width: 65px!important;
                height: 65px!important;
            }
        }
        .left {
            min-width: 125px;
            svg {
                fill: #FFBE0B;
            }
        }
        .right {
            .title{
                font-weight: 300;
                text-transform: none;
                font-size: 30px;
            }
        }
    }

`;
