'use client';
import DoorImpactIcon from '#/app/contants/icon/DoorImpactIcon';
import HungWindow from '#/app/contants/icon/HungWindow';
import SkyLightWindow from '#/app/contants/icon/SkyLightWindow';
import SlidingWindow from '#/app/contants/icon/SlidingWindow';
import TlitTurn from '#/app/contants/icon/TlitTurn';
import TransomWindow from '#/app/contants/icon/TransomWindow';
import React from 'react';
import { WrapUIWhyWeAreBest } from './style';


interface UIWhyWeAreBestProps {
}

const bestArr = [
  {
    icon: <DoorImpactIcon />,
    title: 'ENTRANCE DOORS',
    desc: 'Viverra ipsum nunc aliquet bibendum enim facilisis volutpat blandit aliquam etiam erat velit scelerisque viverra mauris in aliquam.'
  },
  {
    icon: <TlitTurn />,
    title: 'TILT & TURN',
    desc: 'Lobortis mattis aliquam faucibus purus eget nullam non nisi est ipsum faucibus vitae aliquet nec ullamcorper sit amet.'
  },
  {
    icon: <SlidingWindow />,
    title: 'SLIDING WINDOWS',
    desc: 'Dolor sed viverra ipsum nunc aliquet bibendum enim facilisis gravida sagittis purus sit amet volutpat consequat massa.'
  },
  {
    icon: <HungWindow />,
    title: 'HUNG WINDOW',
    desc: 'Dolor sed viverra ipsum nunc aliquet bibendum enim facilisis gravida sagittis purus sit amet volutpat consequat massa.'
  },
  {
    icon: <SkyLightWindow />,
    title: 'SKYLIGHT WINDOW',
    desc: 'Dolor sed viverra ipsum nunc aliquet bibendum enim facilisis gravida sagittis purus sit amet volutpat consequat massa.'
  },
  {
    icon: <TransomWindow />,
    title: 'TRANSOM WINDOW',
    desc: 'Cras sed felis eget velit aliquet sagittis id consectetur purus semper feugiat nibh sed pulvinar proin gravida cum sociis.'
  },

]

const UIWhyWeAreBest: React.FC<UIWhyWeAreBestProps> = ({
}) => (
  <WrapUIWhyWeAreBest>
    {
      bestArr.map((item, index) => (
        <div key={index} className='box-item flex sm:w-full md:w-1/2 xl:w-1/3 p-[30px] relative max-md:px-0'>
          <div className='number'>0{index + 1}</div>
          <div className='left w-1/4 icon'>
            {item.icon}
          </div>
          <div className='right'>
            <h5 className='title mb-5'>{item.title}</h5>
            <h6>{item.desc}</h6>
          </div>
        </div>

      ))
    }
  </WrapUIWhyWeAreBest>
);

export default UIWhyWeAreBest;
