'use client';
import BestHandyManIcon from '#/app/contants/icon/BestHandyManIcon';
import React, { useEffect } from 'react';

import { WrapUIImageUpDownSide } from './style';
interface UIImageUpDownSideProps { }

const UIImageUpDownSide: React.FC<UIImageUpDownSideProps> = ({ }) => {
  useEffect(() => {
    if (typeof window !== 'undefined') {
      document.body.style.setProperty('--scroll', '0.0300752');
      window.addEventListener(
        'scroll',
        () => {
          const ratio = (
            window.pageYOffset /
            (document.body.offsetHeight - window.innerHeight)
          ).toString();
          document.body.style.setProperty('--scroll', ratio || '0.0300752');
        },
        false,
      );
    }
  }, []);

  return (
    <WrapUIImageUpDownSide className="relative flex">
      <div className="wrap-image left w-7/12">
        <img
          src="https://vindors.wpengine.com/wp-content/uploads/2023/05/about-image-1.webp"
          alt=""
        />
        <div className="icon-handyman">
          <BestHandyManIcon />
        </div>
      </div>

      <div className="wrap-image right w-5/12">
        <img
          src="https://vindors.wpengine.com/wp-content/uploads/2023/05/about-image-2.webp"
          alt=""
        />
      </div>
    </WrapUIImageUpDownSide>
  );
};

export default UIImageUpDownSide;
