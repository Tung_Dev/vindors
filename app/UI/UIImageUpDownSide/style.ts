'use client';

import styled from 'styled-components';

export const WrapUIImageUpDownSide = styled.div`
  position: relative;

  @keyframes rotate {
    from {
      transform: rotate(-360deg);
    }
    to {
      transform: rotate(360deg);
    }
  }

  .wrap-image {
    top: 0;
    position: absolute;
    animation-iteration-count: 1;
    animation-fill-mode: both;
    animation-delay: 1;
  }
  .wrap-image {
    position: relative;
    &.left {
      transition: transform 2s ease;
      animation: all 2 ease forwards;
      transform: translateY(calc(var(--scroll) * 300px + 100px));

      position: relative;
      padding-right: 20px;
      z-index: 2;
    }
    &.right {
      transition: transform 2s ease;
      animation: all 2 ease forwards;
      transform: translateY(calc(var(--scroll) * -100px));
    }
  }
  .icon-handyman {
    transition: transform 2s ease;
    position: absolute;
    right: 0;
    top: 25%;
    animation: all 2 ease forwards;

    transform: translate(45%, 0) rotate(calc(var(--scroll) * 200deg));
  }
  @media only screen and (max-width: 767px) {
    .icon-handyman {
      right: 3px;
      svg{
        width: 90px;
        height: 90px;
      }
    }
  }
`;
