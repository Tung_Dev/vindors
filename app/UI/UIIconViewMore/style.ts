'use client';

import styled from 'styled-components';
import { primaryColor } from '../../contants/css';

export const WrapUIIconViewMore = styled.div`
    border-right: 1px solid #d9d9d9;
    &:last-child{
      border-right: none;
    }
  span.icon {
    svg {
      width: 120px;
      height: 120px;
    }
  }
  .box-icon {
    @keyframes showView {
      0% {
        transform: translateX(-50px);
        opacity: 0;
      }
      100% {
        transform: translateX(0);
        opacity: 1;
      }
    }
    &:hover {
      .view-more {
        animation: showView 0.3s ease-in forwards;
        a {
          color: ${primaryColor}!important;
        }
      }
      svg {
        fill: ${primaryColor};
      }
    }
    h5 {
      a:hover {
        color: ${primaryColor};
      }
    }
  }
  .view-more {
    width: 100%;
    opacity: 0;
    animation: all 0.3s ease-in;
  }
  
`;
