import React, { ReactNode } from 'react';
import { WrapUIIconViewMore } from './style';
import { ArrowRightOutlined } from '@ant-design/icons';
interface UIIconViewMoreProps {
  link?: string;
  icon?: ReactNode;
  name?: string;
  className?: string;
  viewMore?: boolean;
}

const UIIconViewMore: React.FC<UIIconViewMoreProps> = ({
  link,
  icon,
  name,
  viewMore = true,
  className = '',
}) => (
  <WrapUIIconViewMore
    className={`impact-icon-product center w-1/3 max-md:mb-10 max-md:w-full ${className}`}
  >
    <a className="cursor-pointer" href={link}>
      <div className="center box-icon relative flex-col">
        <span className="icon">{icon}</span>
        <h5 className="mt-10 break-words text-center text-[14px] font-light uppercase tracking-[2.5px]">
          <p>{name}</p>
        </h5>
        {viewMore && (
          <div className="view-more center mt-5">
            <a
              href={link}
              className="letter-spacing-[8px] text-xs font-semibold uppercase text-[#0060FF]"
            >
              VIEW MORE <ArrowRightOutlined rev={undefined} />
            </a>
          </div>
        )}
      </div>
    </a>
  </WrapUIIconViewMore>
);

export default UIIconViewMore;
