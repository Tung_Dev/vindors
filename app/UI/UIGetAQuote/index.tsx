'use client';
import React from 'react';
import { WrapUIGetAQuote } from './style';
import InputBase from '#/app/components/InputBase';
import { Input } from 'antd';
interface UIGetAQuoteProps { }

const UIGetAQuote: React.FC<UIGetAQuoteProps> = ({ }) => (
  <WrapUIGetAQuote className="">
    <div className="p-20 pb-10 max-lg:p-1">
      <div className="grid grid-cols-[repeat(auto-fill,minmax(48%,1fr))] gap-10">
        <div>
          <InputBase placeholder="Adress Name*" />
          <InputBase placeholder="Adress*" />
        </div>
        <div>
          <Input.TextArea placeholder="Additional Message" />
        </div>
      </div>
      <button className="button-primary m-auto mt-3 h-10 w-[220px]">
        GET A QUOTE
      </button>
    </div>
  </WrapUIGetAQuote>
);

export default UIGetAQuote;
