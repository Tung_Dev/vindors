'use client';

import styled from 'styled-components';

export const WrapUIGetAQuote = styled.div`
  textarea.ant-input {
    resize: none;
    height: 154px;
  }
`;
