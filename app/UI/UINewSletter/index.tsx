'use client';
import React from 'react';
import { WrapUINewSletter } from './style';
import InputBase from '#/app/components/InputBase';
interface UINewSletterProps {}

const UINewSletter: React.FC<UINewSletterProps> = ({}) => (
  <WrapUINewSletter className="">
    <div className="p-20 pb-10 max-lg:p-1">
      <div className="grid grid-cols-[repeat(auto-fill,minmax(30%,1fr))] gap-10 max-lg:flex max-lg:flex-wrap">
        <div className="max-lg:w-full">
          <InputBase placeholder="First Name*" />
        </div>
        <div className="max-lg:w-full">
          <InputBase placeholder="Last Name" />
        </div>
        <div className="max-lg:w-full">
          <InputBase placeholder="Email*" />
        </div>
      </div>
      <button className="button-primary m-auto mt-3 h-10 w-[220px]">
        SEND MESSAGE
      </button>
    </div>
  </WrapUINewSletter>
);

export default UINewSletter;
